
/* 
 * File:   lab 5.cpp
 * Author: Heidi Dye
 *
 * Created on April 30, 2019, 5:52 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
//classes
class NumDays
{
private:
    int numberOfWorkHours;
    float numberOfDays;
public:
    //default constructor
    NumDays();
    //constructor
    NumDays(int numberOfWorkHours);
    //copy constructor
    NumDays(const NumDays&);
    //adding two NumDays objects
    NumDays operator+(const NumDays&);
    //subtracting 
    //setter
    void setNumberOfWorkHours(int numberOfWorkHours);
    void setNumberOfDays(float numberOfDays);
    //getter
    int getNumberOfWorkHours();
    float getNumberOfDays();
};
int main(int argc, char** argv) {

    //create two objects of the NumDays class
    NumDays numDays1(16);
    NumDays numDays2(8);
    NumDays numDays3 = numDays1 + numDays2;
    cout<<numDays3.getNumberOfWorkHours();
    return 0;
}

//default constructor
NumDays::NumDays()
{
    numberOfWorkHours = 0;
    numberOfDays = 0;
}

//constructor
NumDays::NumDays(int numberOfWorkHours)
{
    this->numberOfWorkHours= numberOfWorkHours;
    this->numberOfDays = numberOfWorkHours / 8.0;
}

//copy constructor
NumDays::NumDays(const NumDays& numDays)
{
    this->numberOfWorkHours = numDays.numberOfWorkHours;
    this->numberOfDays = numDays.numberOfDays;
}

//adding two NumDays together
NumDays NumDays::operator +(const NumDays& rhs)
{
    //return the added hours
    int newNumberOfWorkHours = this->numberOfWorkHours + rhs.numberOfWorkHours;
    //float newNumberOfDays = newNumberOfWorkHours/8;
    //return newNumberOfDays;
    return newNumberOfWorkHours;
}

//setter member functions
void NumDays::setNumberOfWorkHours(int numberOfWorkHours)
{
    this->numberOfWorkHours = numberOfWorkHours;
}
void NumDays::setNumberOfDays(float numberOfDays)
{
    this->numberOfDays = numberOfWorkHours/8.0;
}

//getter member functions
int NumDays::getNumberOfWorkHours()
{
    return numberOfWorkHours;
}
float NumDays::getNumberOfDays()
{
    return numberOfDays;
}
