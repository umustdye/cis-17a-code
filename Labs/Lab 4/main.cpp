

/* 
 * File:   main.cpp
 * Author: Heidi Dye
 *
 * Created on April 25, 2019, 7:44 PM
 */

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

class RetailItem
{
private:
    string description;
    int unitsOnHand;
    double price;
public:
    RetailItem();
    RetailItem(string description, int unitsOnHand, double price);
    //getters
    string getDescription();
    int getUnitsOnHand();
    double getPrice();
};

class TestScores
{
private:
double testScore1;
double testScore2;
double testScore3;
public:
    
    
};

int main(int argc, char** argv) {

    RetailItem item1 ("Jacket", 12, 59.95);
    RetailItem item2 ("Designer Jeans", 40, 34.95);
    RetailItem item3 ("Shirt", 20, 24.95);
    //demonstrate outputting the items
    cout << item1.getDescription() << endl;
    cout << item1.getUnitsOnHand() << endl;
    cout << item1.getPrice() << endl;
            
    return 0;
}

RetailItem::RetailItem(string description, int unitsOnHand, double price)
{
    this->description = description;
    this-> unitsOnHand = unitsOnHand;
    this->price = price;
}

string RetailItem::getDescription()
{
    return description;
}

int RetailItem::getUnitsOnHand()
{
    return unitsOnHand;
}

double RetailItem:: getPrice()
{
    return price;
}
    
    
