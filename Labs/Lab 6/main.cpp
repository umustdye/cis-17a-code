
/* 
 * File:   lab 5.cpp
 * Author: Heidi Dye
 *
 * Created on April 30, 2019, 5:52 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
//classes
class Month
{
private:
    string name;
    int monthNumber;
public:
    //default constructor
    Month();
    //1st constructor
    Month(string name);
    //2nd constructor
    Month(int MonthNumber);
    //copy constructor
    Month(const Month&);
    //pre-increment
    Month operator++();
    //post-increment
    Month operator++(int);
    //pre-decrement
    Month operator--();
    //post-decrement
    Month operator--(int);
    //setter
    void setName(string name);
    void setMonthNumber(int monthNumber);
    //getter
    string getName();
    int getMonthNumber();
};
//enum
enum Month{
    JAN, FEB, MAR, APR, MAY, JUN, JULY, AUG, SEPT, OCT, NOV, DEC
};
int main(int argc, char** argv) {


    return 0;
}
//default constructor
Month::Month()
{
    monthNumber = 1;
    name = "January";
}

Month::Month(string name)
{
    
}

Month::Month(int MonthNumber)
{
    
}