
/* 
 * File:   lab7.cpp
 * Author: Heidi Dye
 *
 * Created on May 16, 2019, 7:35 PM
 */

#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

class PersonData
{
private:
    string lastName;
    string firstName;
    string address;
    string city;
    string state;
    string zip;
    string phone;
    
public:
    PersonData();
    PersonData(string, string, string, string, string, string, string);
    //setters
    void setLastName(string);
    void setFirstName(string);
    void setAddress(string);
    void setCity(string);
    void setState(string);
    void setZip(string);
    void setPhone(string);
    
    //getters
    string getLastName();
    string getFirstName();
    string getAddress();
    string getCity();
    string getState();
    string getZip();
    string getPhone();
    
    void output();
};

class CustomerData : public PersonData
{
private:
    int customerNumber;
    bool mailingList;
public:
    CustomerData();
    CustomerData(int, bool, string, string, string, string, string, string, string);
    //setters
    void setCustomerNumber(int);
    void setMailingList(bool);
    
    //getters
    int getCustomerNumber();
    bool getMailingList();
    
    void output();
};
int main(int argc, char** argv) {

    
    CustomerData c (15,true, "Baker", "Jim", "0000 Some Street", "City", "Some State", "00000", "(000)-000-0000");
    c.output();
    return 0;
}

//constructor
PersonData::PersonData()
{
    lastName = "";
    firstName = "";
    address = "";
    city = "";
    state = "";
    zip = "";
    phone = "";
}

PersonData::PersonData(string lastName, string firstName, string address, string city, string state, string zip, string phone)
{
    this->lastName = lastName;
    this->firstName = firstName;
    this->address = address;
    this->city = city;
    this->state = state;
    this->zip = zip;
    this->phone = phone;
}

//setters
void PersonData::setLastName(string lastName)
{
    this->lastName = lastName;
}

void PersonData::setFirstName(string firstName)
{
    this->firstName = firstName;
}

void PersonData::setAddress(string address)
{
    this->address = address;
}

void PersonData::setCity(string city)
{
    this->city = city;
}

void PersonData::setState(string state)
{
    this->state = state;
}

void PersonData::setZip(string zip)
{
    this->zip = zip;
}

void PersonData::setPhone(string phone)
{
    this->phone = phone;
}

//getters

string PersonData::getLastName()
{
    return lastName;
}

string PersonData::getFirstName()
{
    return firstName;
}

string PersonData::getAddress()
{
    return address;
}

string PersonData::getCity()
{
    return city;
}

string PersonData::getState()
{
    return state;
}

string PersonData::getZip()
{
    return zip;
}

string PersonData::getPhone()
{
    return phone;
}

void PersonData::output()
{
    cout<<"Last Name: "<<lastName<<endl;
    cout<<"First Name: "<<firstName<<endl;
    cout<<"Address: "<<address<<endl;
    cout<<"City: "<<city<<endl;
    cout<<"State: "<<state<<endl;
    cout<<"Zip: "<<zip<<endl;
    cout<<"Phone: "<<phone<<endl;
}

//constructors
CustomerData::CustomerData()
        :PersonData()
{
    customerNumber = 0;
    mailingList = false;
}

CustomerData::CustomerData(int customerNumber, bool mailingList, string lastName, string firstName, string address, string city, string state, string zip, string phone)
        :PersonData(lastName, firstName, address, city, state, zip, phone)
{
    this->customerNumber = customerNumber;
    this->mailingList = mailingList;
}

//setters
void CustomerData::setCustomerNumber(int customerNumber)
{
    this->customerNumber = customerNumber;
}

void CustomerData::setMailingList(bool mailingList)
{
    this->mailingList = mailingList;
}

//getters

int CustomerData::getCustomerNumber()
{
    return customerNumber;
}

bool CustomerData::getMailingList()
{
    return mailingList;
}

void CustomerData::output()
{
    PersonData::output();
    cout<<"Customer Number: "<<customerNumber<<endl;
    string ifOnML;
    if(getMailingList == true)
    {
        ifOnML = "Yes";
    }
    else
    {
        ifOnML = "No";
    }
    
    cout<<"Mailing List: "<<ifOnML<<endl;
}