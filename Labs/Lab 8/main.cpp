
/* 
 * File:   Lab 8.cpp
 * Author: Heidi Dye
 *
 * Created on May 21, 2019, 7:23 PM
 */

#include <cstdlib>
#include <iomanip>
#include <iostream>

using namespace std;
template<class T>
void output(T);


class Rectangle
{
    private:
        double length;
        double width;
        public:
            Rectangle();
            Rectangle(double, double);
            void setLength(double);
            void setWidth(double);
            void output();
            friend ostream& operator<<(ostream&, const Rectangle&);
};
int main(int argc, char** argv) {

    double width  = 5.6;
    int length = 3;
    Rectangle r(length, width);
    r.output();
    output(r);
    output(0);
    output(5.5);
    return 0;
}

//default constructor
Rectangle::Rectangle()
{
    length = 0.0;
    width = 0.0;
}

//constructor
Rectangle::Rectangle(double length, double width)
{
    this->length = length;
    this->width = width;
    
}


void Rectangle::setLength(double length)
{
    this->length = length;
}


void Rectangle::setWidth(double width)
{
    this->width = width;
}


void Rectangle::output()
{
    cout << "Length: " << length << endl;
    cout << "Width: " << width << endl;
}


ostream& operator<<(ostream& out, const Rectangle& rectangle)
{
    out << rectangle.length << endl;
    out << rectangle.width << endl;
    return out;
}

template<class T>
void output(T input)
{
    cout << input << endl;
}


