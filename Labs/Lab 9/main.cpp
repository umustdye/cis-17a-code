
/* 
 * File:   lab 9.cpp
 * Author: Heidi Dye
 *
 * Created on May 23, 2019, 7:30 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
//structs
struct Node
{
    int value;
    Node* next;
};
//classes
class LinkedList
{
private:
    Node* head;
public:
    LinkedList();
    ~LinkedList();
    void append(int val);
    void insert(int val);
    void remove(int val);
    bool search(int val);
    void print();
};
int main(int argc, char** argv) {

    return 0;
}

//constructor
LinkedList::LinkedList()
{
    head = NULL;
}

//destructor
LinkedList::~LinkedList()
{
    //traversal pointer
    Node* nodePtr = head;
    
    while(nodePtr)
    {
        Node* temp = nodePtr;
        nodePtr = nodePtr->next;
        delete temp;
    }
    
    head = NULL;
}

//copy constructor


//append
void LinkedList::append(int val)
{
    Node* newNode = new Node;
    newNode->value = val;
    newNode->next = NULL;
    
    if(!head)
    {
        head = newNode;
    }
    
    else
    {
        Node* nodePtr = head;
        while(nodePtr->next)
        {
        
            nodePtr = nodePtr->next;
        }
        nodePtr->next = newNode;
                
    }
}

void LinkedList::insert(int val)
{
    Node* newNode = new Node;
    newNode->value = val;
    Node* nodePtr = head;
    Node* previousNode;
    Node* temp;
    while(nodePtr)
    {
        previousNode = nodePtr;
        nodePtr = nodePtr->next;
        if((newNode->value > previousNode) && (newNode->value < nodePtr))
        {
            temp = nodePtr;
            nodePtr = newNode;
        }
                
    }
}

void LinkedList::remove(int val)
{

}

bool LinkedList::search(int val)
{
    Node* nodePtr = head;
    while(nodePtr)
    {
        if(nodePtr->value == val)
        {
            return true;
        }
        nodePtr = nodePtr->next;
    }
    return false;
}