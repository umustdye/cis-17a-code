/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DynamicQueue.h
 * Author: Heidi2
 *
 * Created on June 4, 2019, 11:06 PM
 */

#ifndef DYNAMICQUEUE_H
#define DYNAMICQUEUE_H

#include <iostream>

using namespace std;

//Node
struct Node1
{
    int val;
    Node1* next;
};

class DynamicQueue {
public:
    DynamicQueue();
    DynamicQueue(const DynamicQueue& orig);
    virtual ~DynamicQueue();
    void output();
    void enqueue(int val);
    void dequeue();
private:
    Node1* front;
    Node1* rear;

};

#endif /* DYNAMICQUEUE_H */

