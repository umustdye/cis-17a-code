
/* 
 * File:   Lab 10.cpp
 * Author: Heidi Dye
 *
 * Created on May 28, 2019, 7:47 PM
 */

#include <cstdlib>
#include <iostream>
#include "StaticStack.h"
#include "DynamicStack.h"
#include "StaticQueue.h"
#include "DynamicQueue.h"
#include "DynamicStringStack.h"

using namespace std;


int main(int argc, char** argv) {
    
    //demonstrate the usage of the static stack
    //get the size or capacity from the user
    cout << "What capacity do you desire your static stack to be?"<<endl;
    int capacity;
    cin >>capacity;
    //create an instance of the StaticStack class and call the constructor
    StaticStack staticStack(capacity);
    //have the user push values to fill the array 
    //demonstrate the usage of the Stack being full as well
    int loopCounter = 0;
    int dummyInput = 0;
    while(loopCounter<=capacity)
    {
        cout << "Enter the value you wish to push to the stack: ";
        cin >> dummyInput;
        staticStack.push(dummyInput);
        loopCounter++;
    }
    //output the stack
    staticStack.output();
    //pop a value form the stack
    staticStack.pop();
    //output the stack
    staticStack.output();
    
    
    //Demonstrate the Dynamic Stack
    //create an instance for the DynamicStack class
    DynamicStack dynamicStack;
    //push 5 values to the stack
    dynamicStack.push(1);
    dynamicStack.push(2);
    dynamicStack.push(3);
    dynamicStack.push(4);
    dynamicStack.push(5);
    //output the dynamic stack
    cout<<"Dynamic Stack after pushing: ";
    dynamicStack.output();
    //pop a value
    cout<<"Dynamic Stack after popping: ";
    dynamicStack.pop();
    //output the stack
    dynamicStack.output();
    
    
    //demonstrate the static queue
    //get the capacity from the user
    int capacity1 = 0;
    cout << "Enter the Capacity for the Static Queue: ";
    cin >> capacity1;
    //make an instance of the static queue class
    StaticQueue staticQueue(capacity1);
    //add values to the queue
    //demonstrate the usage of the Queue being full as well
    int loopCounter1 = 0;
    int dummyInput1 = 0;
    while(loopCounter1<=capacity1)
    {
        cout << "Enter the value you wish to enqueue to the queue: ";
        cin >> dummyInput1;
        staticQueue.enqueue(dummyInput1);
        loopCounter1++;
    }
    //output the stack
    cout<<"Queue after enqueue: "<<endl;
    staticQueue.output();
    
    //dequeue
    staticQueue.dequeue();
    //output the stack
    cout<<"Queue after dequeue: "<<endl;
    staticQueue.output();
    
    //Demonstrate the Dynamic queue
    //create an instance for the DynamicQueue class
    DynamicQueue dynamicQueue;
    //enqueue 5 values to the queue
    dynamicQueue.enqueue(1);
    dynamicQueue.enqueue(2);
    dynamicQueue.enqueue(3);
    dynamicQueue.enqueue(4);
    dynamicQueue.enqueue(5);
    //output the dynamic queue
    cout<<"Dynamic Queue after enqueue: ";
    dynamicQueue.output();
    //dequeue a value
    cout<<"Dynamic Queue after dequeue: ";
    dynamicQueue.dequeue();
    //output the queue
    dynamicQueue.output();
    
    
    //Demonstrate the Dynamic String Stack
    //create an instance for the DynamicStringStack class
    DynamicStringStack dynamicStringStack;
    //push 5 values to the stack
    dynamicStringStack.push("apple");
    dynamicStringStack.push("banana");
    dynamicStringStack.push("George");
    dynamicStringStack.push("Cat");
    dynamicStringStack.push("Wig");
    //output the dynamic string stack
    cout<<"Dynamic String Stack after pushing: ";
    dynamicStringStack.output();
    //pop a value
    cout<<"Dynamic String Stack after popping: ";
    dynamicStringStack.pop();
    //output the stack
    dynamicStringStack.output();        
    return 0;
}

