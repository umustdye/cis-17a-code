/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StaticStack.cpp
 * Author: Heidi Dye
 * 
 * Created on May 28, 2019, 7:49 PM
 */

#include "StaticStack.h"
#include <iostream>

using namespace std;

StaticStack::StaticStack(int capacity) 
{
    this->capacity = capacity;
    this->stack = new int[capacity];
    this->top = -1;
}

/*StaticStack::StaticStack(const StaticStack& orig) {
}*/
void StaticStack::push(int val)
{
    //check if full
    if (top == capacity - 1)
    {
        cout << "The stack is full..." << endl;
    }
    
    else
    {
        top++;
        stack[top] = val;
    }
}

void StaticStack::pop()
{
    if(isEmpty())
    {
        cout << "The stack is empty..." << endl;
    }
    
    else
    {
        top--;
    }
}

bool StaticStack::isEmpty()
{
    return top == -1;
}

StaticStack::~StaticStack() 
{
    if (stack)
    {
        delete[]stack;
        stack = NULL;
    }
}

void StaticStack::output()
{
    for (int i = 0; i<=top; i++)
    {
        cout <<stack[i]<<" ";
    }
    cout << endl;
}