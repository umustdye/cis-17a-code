/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StaticQueue.h
 * Author: Heidi2
 *
 * Created on June 4, 2019, 10:25 PM
 */

#ifndef STATICQUEUE_H
#define STATICQUEUE_H
#include <iostream>

using namespace std;

class StaticQueue {
public:
    StaticQueue();
    StaticQueue(const StaticQueue& orig);
    virtual ~StaticQueue();
    StaticQueue(int); //need a size
    void enqueue(int); //add a value to the queue
    int dequeue(); //remove from the queue
    void output();
private:
    int* queue; //static queue
    int size; //Number of current values
    int capacity; // total size of the queue
    int front; // location of the front
    int rear; //location of the rear

};

#endif /* STATICQUEUE_H */

