/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DynamicStringStack.h
 * Author: Heidi2
 *
 * Created on June 4, 2019, 11:37 PM
 */

#ifndef DYNAMICSTRINGSTACK_H
#define DYNAMICSTRINGSTACK_H
#include <iostream>
#include <new>
using namespace std;

struct Node2
{
    string value;
    Node2* next;
};

class DynamicStringStack {
public:
    DynamicStringStack();
    //DynamicStack(const DynamicStack& orig);
    virtual ~DynamicStringStack();
    void pop();
    void push(string val);
    void output();
private:
    Node2* top;

};

#endif /* DYNAMICSTRINGSTACK_H */

