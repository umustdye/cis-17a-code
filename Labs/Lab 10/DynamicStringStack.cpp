

/* 
 * File:   DynamicStringStack.cpp
 * Author: Heidi2
 * 
 * Created on June 4, 2019, 11:37 PM
 */

#include "DynamicStringStack.h"

/*DynamicStringStack::DynamicStringStack() {
}

DynamicStringStack::DynamicStringStack(const DynamicStringStack& orig) {
}

DynamicStringStack::~DynamicStringStack() {
}
*/
using namespace std;

DynamicStringStack::DynamicStringStack() 
{
    top = NULL;
}

/*DynamicStack::DynamicStack(const DynamicStack& orig) {
}*/

DynamicStringStack::~DynamicStringStack() 
{
    Node2* nodePtr = top;
    
    while(nodePtr)
    {
        Node2* temp = nodePtr;
        nodePtr = nodePtr->next;
        delete temp;
    }
    
    top = NULL;
}

void DynamicStringStack::output()
{
    Node2* nodePtr = top;
    
    while(nodePtr)
    {
        cout << nodePtr->value<< " ";
        nodePtr = nodePtr->next;
    }
    cout<<endl;
}

void DynamicStringStack::pop()
{
    //empty check
    if(!top)return;
    //temp pointer
    Node2* temp = top;
    top = top->next;
    delete temp;
}

void DynamicStringStack::push(string value)
{
    //step 1
    try
    {
        Node2* newNode = new Node2;
        
        newNode->value = value;
        newNode->next = NULL;
    
        if(top)
        {
            newNode->next = top;
        }
    
        top = newNode;
    }
    
    
    
  catch (std::bad_alloc& ba)
  {
    std::cerr << "bad_alloc caught: " << ba.what() << '\n';
  }
}
