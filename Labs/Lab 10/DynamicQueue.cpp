/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DynamicQueue.cpp
 * Author: Heidi2
 * 
 * Created on June 4, 2019, 11:06 PM
 */

#include "DynamicQueue.h"

DynamicQueue::DynamicQueue() 
{
    rear = front = NULL;
}

DynamicQueue::DynamicQueue(const DynamicQueue& orig) {
}

DynamicQueue::~DynamicQueue() {
}

//adds/appends a value at the end of the queue
void DynamicQueue::enqueue(int val)
{
    try
    {
        //first create the node
        Node1* newNode = new Node1;
        newNode->val = val;
        newNode->next = NULL;
    
        //check id front is NULL
        //if rear is null, then nothing is in the queue
        if(!rear)
        {
            front = rear = newNode;
        }
    
        else
        {
            rear->next = newNode;
            rear = newNode;
        }
    }
    
      catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
}

void DynamicQueue::dequeue()
{
    //error checking
    //make sure value exist
    if(!front)
    {
        //reset
        rear = NULL; //nothing to remove
        return;
    }
    
    else
    {
        //temp pointer
        Node1* temp = front;
        
        //move front over
        front = front->next;

        //check to see if null
        if(!front)
        {
            rear = NULL;
        }
        //deallocation
        delete temp;
    }
}

//print all values in queue to screen
void DynamicQueue::output()
{
    
    //traversal
    Node1* nodePtr = front;
    
    while(nodePtr)
    {
        std::cout<<nodePtr->val<<" ";
        //traverse
        nodePtr = nodePtr->next;
    }
    std::cout<<std::endl;
}