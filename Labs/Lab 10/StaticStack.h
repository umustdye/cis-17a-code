/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StaticStack.h
 * Author: rcc
 *
 * Created on May 28, 2019, 7:49 PM
 */

#ifndef STATICSTACK_H
#define STATICSTACK_H

class StaticStack {
public:
    StaticStack(int capacity);
    //StaticStack(const StaticStack& orig);
    virtual ~StaticStack();
    void push(int);
    void pop();
    void output();
    
private:
    int top;
    int capacity;
    int* stack;
    bool isEmpty();
};

#endif /* STATICSTACK_H */

