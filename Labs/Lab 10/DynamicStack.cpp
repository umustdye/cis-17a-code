/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DynamicStack.cpp
 * Author: Heidi Dye
 * 
 * Created on May 28, 2019, 8:10 PM
 */

#include "DynamicStack.h"
#include <iostream>

using namespace std;

DynamicStack::DynamicStack() 
{
    top = NULL;
}

/*DynamicStack::DynamicStack(const DynamicStack& orig) {
}*/

DynamicStack::~DynamicStack() 
{
    Node* nodePtr = top;
    
    while(nodePtr)
    {
        Node* temp = nodePtr;
        nodePtr = nodePtr->next;
        delete temp;
    }
    
    top = NULL;
}

void DynamicStack::output()
{
    Node* nodePtr = top;
    
    while(nodePtr)
    {
        cout << nodePtr->value<< " ";
        nodePtr = nodePtr->next;
    }
    cout<<endl;
}

void DynamicStack::pop()
{
    //empty check
    if(!top)return;
    //temp pointer
    Node* temp = top;
    top = top->next;
    delete temp;
}

void DynamicStack::push(int value)
{
    try
    {
        //step 1
        Node* newNode = new Node;
        newNode->value = value;
        newNode->next = NULL;
    
        if(top)
        {
            newNode->next = top;
        }   
    
        top = newNode;
    }
    
      catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
}