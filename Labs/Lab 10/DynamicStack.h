

/* 
 * File:   DynamicStack.h
 * Author: Heidi Dye
 *
 * Created on May 28, 2019, 8:10 PM
 */

#ifndef DYNAMICSTACK_H
#define DYNAMICSTACK_H


struct Node
{
    int value;
    Node* next;
};

class DynamicStack {
public:
    DynamicStack();
    //DynamicStack(const DynamicStack& orig);
    virtual ~DynamicStack();
    void pop();
    void push(int val);
    void output();
private:
    Node* top;

};

#endif /* DYNAMICSTACK_H */

