

/* 
 * File:   Homework 3.cpp
 * Author: Heidi Dye
 *
 * Created on April 26, 2019, 5:03 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <ctime>
#include <cstring>

using namespace std;

//struct for number three Movie Data
struct MovieData
{
    string title;
    string director;
    string yearReleased;
    string runningTime;
    
};

//struct for number four Movie Profit
struct MovieProfit
{
    string title;
    string director;
    string yearReleased;
    string runningTime;
    long long int productionCost;
    long long int firstYearRevenue;
};

//struct for Weather Statistics
struct WeatherStatistics
{
    double totalRainfall;
    double highTemp;
    double lowTemp;
    double averageTemp;
};

//function declarations
void one();
void two();
void three();
void movieDataOutput(MovieData);
void four();
void movieProfitOutput(MovieProfit);
void five();
void ifValidTemp(WeatherStatistics);

int main(int argc, char** argv) {

    one();
    cout << endl;
    two();
    cout << endl;
    three();
    cout << endl;
    four();
    cout << endl;
    five();
    cout << endl;
    return 0;
}

void one()
{
    //pg 609
    cout << "#1: Date Printer" << endl;
    //for the date mm/dd/yyyy
    string userdate;
    
    //get the date from the user
    //example of user input "04/26/2019"
    cout << "Enter the date (mm/dd/yyyy): ";
    cin >> userdate;
    
    //separate the userdate in parts
    string month = userdate;
    month = month.substr(0,2);
    string day = userdate;
    //for some odd reason I had to change the end of the string to 2. It wouldn't work with (3, 5).
    day = day.substr(3,2);
    string year = userdate;
    year = year.substr(6,9);
    
    //change month into an int for comparing reasons
    int monthint = stoi(month);
    
     //switch the month number into the word of the month
    switch (monthint)
    {
        case 1: 
            month = "January";
            break;
        case 2:
            month = "February";
            break;
        case 3: 
            month = "March";
            break;
        case 4:
            month = "April";
            break;
        case 5:
            month = "May";
            break;
        case 6:
            month = "June";
            break;
        case 7:
            month = "July";
            break;
        case 8:
            month = "August";
            break;
        case 9:
            month = "September";
            break;
        case 10:
            month = "October";
            break;
        case 11:
            month = "November";
            break;
        case 12:
            month = "December";
            break;
    }
   
    //output the date
    cout <<month<<" "<<day<<", "<<year<<endl;
}

void two()
{
    //pg 608
    cout << "#2: Name Arranger" << endl;
    
    //get the first name from the user
    char firstname[100];
    cout << "Enter your first name: ";
    cin >> firstname;
    
    //get the middle name from the user
    char middlename[100];
    cout << "Enter your middle name: ";
    cin >> middlename;
    
    //get the last name from the user
    char lastname[100];
    cout << "Enter your last name: ";
    cin >> lastname;
    
    
    //save the sizes of each part of the user's name into an integer variable
    int firstnamesize = strlen(firstname);
    int middlenamesize = strlen(middlename);
    int lastnamesize = strlen(lastname);
    int fullnamesize = firstnamesize + middlenamesize + lastnamesize + 3;
    
    //make the new array that holds the form "Last, First middle"
    string fullname[fullnamesize];
    
    //put the last name first in the array
    for (int i = 0; i<lastnamesize; i++)
    {
        fullname[i] = lastname[i];
    }
    
    // add a comma and a space after the last name
    fullname[lastnamesize] = ",";
    fullname[lastnamesize + 1] = " ";
    
    //insert the first name in the full name array
    for (int i = 0; i <firstnamesize; i++ )
    {
        fullname[lastnamesize +2 +i] = firstname[i];
    }
    
    // add a space after the first name
    fullname[firstnamesize + lastnamesize + 2] = " ";
    
    
    //insert the last name in the fullname array
    for (int i = 0; i<middlenamesize; i++)
    {
        fullname[lastnamesize + firstnamesize + 3 +i] = middlename[i];
    }
    
    //output the full name
    for (int i = 0; i <fullnamesize; i++)
    {
        cout << fullname[i];
    }
    cout <<endl;
}

void three()
{
    //page 659
    cout << "#3: Movie Data" << endl;
    // 1st MovieData variable
    MovieData movie1;
    
    //2nd MovieData Variable
    MovieData movie2;
    
    //If you want values pre-chosen for the MovieData variables
    
    //first movie's data
    movie1.title = "The Dark Knight";
    movie1.director = "Christopher Nolan";
    movie1.yearReleased = "2008";
    movie1.runningTime = "152";
    
    //second movie's data
    movie2.title = "The Room";
    movie2.director = "Tommy Wiseau";
    movie2.yearReleased = "2003";
    movie2.runningTime = "99";
    //movie1 = ("The Dark Knight", "Christopher Nolan", "2008", "152");
    //movie2 = ("The Room", "Tommy Wiseau", "2003", "99");
    
    //If you want the user to enter the values for the MovieData variables
    /*
    //first movie's data
    cout << "Enter the title of the first movie: ";
    cin >> movie1.title;
    cout << "Enter the director of the first movie: ";
    cin >> movie1.director;
    cout << "Enter the year the first movie was released: ";
    cin >> movie1.yearReleased;
    cout << "Enter the running time of the first movie (in minutes): ";
    cin >> movie1.runningTime;
    
    //second movie's data
    cout << "Enter the title of the second movie: ";
    cin >> movie2.title;
    cout << "Enter the director of the second movie: ";
    cin >> movie2.director;
    cout << "Enter the year the second movie was released: ";
    cin >> movie2.yearReleased;
    cout << "Enter the running time of the second movie (in minutes): ";
    cin >> movie2.runningTime;
    */
    //output the first movie's data
    movieDataOutput(movie1);
    cout << endl;
    //output the second movie
    movieDataOutput(movie2);
}

void movieDataOutput(MovieData movie)
{
    cout << "Title: "<<movie.title<<endl;
    cout << "Director: " <<movie.director<<endl;
    cout << "Year Released: "<<movie.yearReleased<<endl;
    cout << "Running Time (in minutes): "<<movie.runningTime<<endl;
}

void four()
{
    //pg 659
    cout <<"#4: Movie Profit"<<endl;
    // 1st MovieProfit variable
    MovieProfit movie1;
    
    //2nd MovieProfit Variable
    MovieProfit movie2;
    
    //If you want values pre-chosen for the MovieProfit variables
    
    //first movie's data
    movie1.title = "The Dark Knight";
    movie1.director = "Christopher Nolan";
    movie1.yearReleased = "2008";
    movie1.runningTime = "152";
    movie1.productionCost = 185000000;
    movie1.firstYearRevenue = 533345358;
    
    //second movie's data
    movie2.title = "The Room";
    movie2.director = "Tommy Wiseau";
    movie2.yearReleased = "2003";
    movie2.runningTime = "99";
    movie2.productionCost = 6000000;
    movie2.firstYearRevenue = 1800;
 
    
    //If you want the user to enter the values for the MovieData variables
    /*
    //first movie's data
    cout << "Enter the title of the first movie: ";
    cin >> movie1.title;
    cout << "Enter the director of the first movie: ";
    cin >> movie1.director;
    cout << "Enter the year the first movie was released: ";
    cin >> movie1.yearReleased;
    cout << "Enter the running time of the first movie (in minutes): ";
    cin >> movie1.runningTime;
    cout << "Enter the budget of the first movie: ";
    cin >> movie1.productionCost;
    cout << "Enter the first movie's first-year revenues: ";
    cin >> movie1.firstYearRevenue;
    
    //second movie's data
    cout << "Enter the title of the second movie: ";
    cin >> movie2.title;
    cout << "Enter the director of the second movie: ";
    cin >> movie2.director;
    cout << "Enter the year the second movie was released: ";
    cin >> movie2.yearReleased;
    cout << "Enter the running time of the second movie (in minutes): ";
    cin >> movie2.runningTime;
    cout << "Enter the budget of the second movie: ";
    cin >> movie2.productionCost;
    cout << "Enter the second movie's first-year revenues: ";
    cin >> movie2.firstYearRevenue;
    */
    //output the first movie's data
    movieProfitOutput(movie1);
    cout << endl;
    //output the second movie
    movieProfitOutput(movie2);
}

void movieProfitOutput(MovieProfit movie)
{
    cout << "Title: "<<movie.title<<endl;
    cout << "Director: " <<movie.director<<endl;
    cout << "Year Released: "<<movie.yearReleased<<endl;
    cout << "Running Time (in minutes): "<<movie.runningTime<<endl;
    cout << "First Year's Profit or Loss: "<<movie.firstYearRevenue - movie.productionCost<<endl;
}

void five()
{
    //pg 659
    cout << "#5: Weather Statistics"<<endl;
    //create the weatherstatistics array with 12 elements
    WeatherStatistics weatherStatistics[12];
    double maxRange = 140;
    double minRange = -100;
    
    //fill the array with data
    for (int i =0; i<12; i++)
    {
        //January
        if (i == 0)
        {
            cout << "Enter the following for the month of January..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
        
        //February
        if (i == 1)
        {
            cout << "Enter the following for the month of February..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
        }
        
        //March
        if (i == 2)
        {
            cout << "Enter the following for the month of March..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
        }
        
        //April
        if (i == 3)
        {
            cout << "Enter the following for the month of April..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
        }
        
        //May
        if (i == 4)
        {
            cout << "Enter the following for the month of May..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
        
        //June
        if (i == 5)
        {
            cout << "Enter the following for the month of June..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
        }
        
        //July
        if (i == 6)
        {
            cout << "Enter the following for the month of July..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
        
        //August
        if (i == 7)
        {
            cout << "Enter the following for the month of August..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
            
        //September
        if (i == 8)
        {
            cout << "Enter the following for the month of September..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
        
        //October
        if (i == 9)
        {
            cout << "Enter the following for the month of October..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
        }
        
        //November
        if (i == 10)
        {
            cout << "Enter the following for the month of November..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
        
        //December
        if (i == 11)
        {
            cout << "Enter the following for the month of December..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[i].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[i].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[i].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[i].averageTemp;
    
        }
        //input validation
        if(weatherStatistics[i].highTemp < minRange || maxRange < weatherStatistics[i].highTemp)
        {
            cout << "ERROR! Highest Temperature must be between the range of -100 and 140 degrees Fahrenheit..."<<endl;
            exit(1);
        }
        if(weatherStatistics[i].lowTemp < minRange || weatherStatistics[i].lowTemp > maxRange)
        {
            cout << "ERROR! Lowest Temperature must be between the range of -100 and 140 degrees Fahrenheit..."<<endl;
            exit(1);
        }
        if(weatherStatistics[i].averageTemp < minRange || weatherStatistics[i].averageTemp > maxRange)
        {
            cout << "ERROR! Average Temperature must be between the range of -100 and 140 degrees Fahrenheit..."<<endl;
            exit(1);
            }
    }
    
    //calculate the total rainfall
    double totalRainFall = 0;
    for (int i = 0; i<12; i++)
    {
        totalRainFall = totalRainFall + weatherStatistics[i].totalRainfall;
    }
    
    //calculate total average temperature
    double totalAverageRainFall = 0;
    totalAverageRainFall = totalRainFall/12;
    
    //calculate the highest temp
    double highestTemp = -100;
    int highestTempMonth =0;
    for (int i = 0; i<12; i++)
    {
        if (highestTemp < weatherStatistics[i].highTemp)
        {
            highestTempMonth =i+1;
            highestTemp = weatherStatistics[i].highTemp;
        }
       
    }
    string hTempMonth;
    //switch the int month into a string
    switch (highestTempMonth)
    {
        case 1:
            hTempMonth = "January";
            break;
        case 2:
            hTempMonth = "February";
            break;
        case 3:
            hTempMonth = "March";
            break;
        case 4:
            hTempMonth = "April";
            break;
        case 5:
            hTempMonth = "May";
            break;
        case 6:
            hTempMonth = "June";
            break;
        case 7:
            hTempMonth = "July";
            break;
        case 8:
            hTempMonth = "August";
            break;
        case 9:
            hTempMonth = "September";
            break;
        case 10:
            hTempMonth = "October";
            break;
        case 11:
            hTempMonth = "November";
            break;
        case 12:
            hTempMonth = "December";
            break;
            
    }
    
    //calculate lowest temp
    double lowestTemp = 140;
    int lowestTempMonth = 0;
    for (int i = 0; i<12; i++)
    {
        if (lowestTemp > weatherStatistics[i].lowTemp)
        {
            lowestTempMonth = i+1;
            lowestTemp = weatherStatistics[i].lowTemp;
        }
    }
    
    string lTempMonth;
    //switch the int month into a string
    switch (lowestTempMonth)
    {
        case 1:
            lTempMonth = "January";
            break;
        case 2:
            lTempMonth = "February";
            break;
        case 3:
            lTempMonth = "March";
            break;
        case 4:
            lTempMonth = "April";
            break;
        case 5:
            lTempMonth = "May";
            break;
        case 6:
            lTempMonth = "June";
            break;
        case 7:
            lTempMonth = "July";
            break;
        case 8:
            lTempMonth = "August";
            break;
        case 9:
            lTempMonth = "September";
            break;
        case 10:
            lTempMonth = "October";
            break;
        case 11:
            lTempMonth = "November";
            break;
        case 12:
            lTempMonth = "December";
            break;
            
    }
    
    //calculate average temp
    double totalAverageTemp = 0;
    for (int i = 0; i<12; i++)
    {
        totalAverageTemp = totalAverageTemp + weatherStatistics[i].averageTemp;
    }
    
    totalAverageTemp = totalAverageTemp / 12;
    
    //output the data
    cout<<"Total Rainfall: "<<totalRainFall<<endl;
    cout<<"Average Rainfall: "<<totalAverageRainFall<<endl;
    cout<<"Highest Temperature: "<<highestTemp<<" in the month of "<<hTempMonth<<endl;
    cout<<"Lowest Temperature: "<<lowestTemp<<" in the month of "<<lTempMonth<<endl;
    cout<<"Average Temperature: "<<totalAverageTemp<<endl;
}
 