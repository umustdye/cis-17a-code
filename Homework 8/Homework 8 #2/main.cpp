

/* 
 * File:   Homework 8 #2.cpp
 * Author: Heidi Dye
 *
 * Created on May 20, 2019, 7:03 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

//classes

class Employee
{
private:
    string employeeName;
    string employeeNumber;
    string hireDate;
    
public:
    //default constructor
    Employee();
    Employee(string, string, string);
    //destructor
    ~Employee();
    //copy constructor
    Employee(const Employee&);
    //getter
    string getEmployeeName();
    string getEmployeeNumber();
    string getHireDate();
    //setters
    void setEmployeeName(string employeeName);
    void setEmployeeNumber(string employeeNumber);
    void setHireDate(string hireDate);
    void output();
};

//child class for Employee class
class ShiftSupervisor : public Employee
{
private:
    double annualSalary;
    double annualProductionBonus;
public:
    //default constructor
    ShiftSupervisor();
    //other constructor
    ShiftSupervisor(string employeeName, string employeeNumber, string hireDate, double annualSalary, double annualProductionBonus);
    //destructor
    ~ShiftSupervisor();
    //copy constructor
    ShiftSupervisor(const ShiftSupervisor&);
    
    //setter member functions
    void setAnnualSalary(double);
    void setAnnualProductionBonus(double);
    
    //getter member functions
    double getAnnualSalary();
    double getAnnualProductionBonus();
    
    void output();
};
int main(int argc, char** argv) 
{

    //if you want the user to input the info
    //get the info for the production worker employee
    string employeeName;
    string employeeNumber;
    string hireDate;
    double annualSalary = 0.0;
    double annualProductionBonus = 0.0;
    
    cout <<"Enter the name of the Employee: ";
    getline(cin, employeeName);
    cout <<"Enter the Employee's number: ";
    cin >> employeeNumber;
    cin.ignore();
    cout <<"Enter the date the Employee was hired: ";
    getline(cin, hireDate);
    cout << "Enter the annual salary: ";
    cin >> annualSalary;
    cout <<"Enter the annual production bonus: ";
    cin >> annualProductionBonus;
            
    //make an instance for the ProductionWorker class
    ShiftSupervisor worker(employeeName, employeeNumber, hireDate, annualSalary, annualProductionBonus);
    
    //output the production worker employee information
    worker.output();
    return 0;
    return 0;
}

//Default constructor
Employee::Employee()
{
    employeeName = "";
    employeeNumber = "";
    hireDate = "";
}

//constructor 2.0
Employee::Employee(string employeeName, string employeeNumber, string hireDate)
{
    this->employeeName = employeeName;
    this->employeeNumber = employeeNumber;
    this->hireDate = hireDate;
}

//destructor
Employee::~Employee()
{
    
}

//copy constructor
Employee::Employee(const Employee& employee)
{
    this->employeeName = employee.employeeName;
    this->employeeNumber = employee.employeeNumber;
    this->hireDate = employee.hireDate;
}

//setter member functions
void Employee::setEmployeeName(string employeeName)
{
    this->employeeName = employeeName;
}

void Employee::setEmployeeNumber(string employeeNumber)
{
    this->employeeNumber = employeeNumber;
}

void Employee::setHireDate(string hireDate)
{
    this->hireDate = hireDate;
}

//getter member functions
string Employee::getEmployeeName()
{
    return employeeName;
}

string Employee::getEmployeeNumber()
{
    return employeeNumber;
}

string Employee::getHireDate()
{
    return hireDate;
}

void Employee::output()
{
    cout <<"Employee Name: "<<employeeName<<endl;
    cout<<"Employee Number: "<<employeeNumber<<endl;
    cout<<"Hire Date: "<<hireDate<<endl;
}

//default constructor
ShiftSupervisor::ShiftSupervisor()
        :Employee()
{
    annualSalary = 0.0;
    annualProductionBonus = 0.0;
}

//constructor 2.0
ShiftSupervisor::ShiftSupervisor(string employeeName, string employeeNumber, string hireDate, double annualSalary, double annualProductionBonus)
        :Employee(employeeName, employeeNumber, hireDate)
{
    this->annualSalary = annualSalary;
    this->annualProductionBonus = annualProductionBonus;
}

//destructor
ShiftSupervisor::~ShiftSupervisor()
{
    
}

//copy constructor
ShiftSupervisor::ShiftSupervisor(const ShiftSupervisor& shiftSupervisor)
        :Employee(shiftSupervisor)
{
    this->annualSalary = shiftSupervisor.annualSalary;
    this->annualProductionBonus = shiftSupervisor.annualProductionBonus;
}

//setter member functions
void ShiftSupervisor::setAnnualSalary(double annualSalary)
{
    this->annualSalary = annualSalary;
}

void ShiftSupervisor::setAnnualProductionBonus(double annualProductionBonus)
{
    this->annualProductionBonus = annualProductionBonus;
}

//getter member functions
double ShiftSupervisor::getAnnualProductionBonus()
{
    return annualProductionBonus;
}
double ShiftSupervisor::getAnnualSalary()
{
    return annualSalary;
}

void ShiftSupervisor::output()
{
    Employee::output();
    cout<<"Annual Salary: $"<<fixed<<setprecision(2)<<annualSalary<<endl;
    cout<<"Annual Production Bonus: $"<<fixed<<setprecision(2)<<annualProductionBonus<<endl;
}