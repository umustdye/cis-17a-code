

/* 
 * File:   Homework 8 #1.cpp
 * Author: Heidi Dye
 *
 * Created on May 20, 2019, 7:03 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

//classes

class Employee
{
private:
    string employeeName;
    string employeeNumber;
    string hireDate;
    
public:
    //default constructor
    Employee();
    Employee(string, string, string);
    //destructor
    ~Employee();
    //copy constructor
    Employee(const Employee&);
    //getter
    string getEmployeeName();
    string getEmployeeNumber();
    string getHireDate();
    //setters
    void setEmployeeName(string employeeName);
    void setEmployeeNumber(string employeeNumber);
    void setHireDate(string hireDate);
    void output();
};

//child class for employee
class ProductionWorker: public Employee
{
private:
    int shift;
    double hourlyPayRate;
public:
    //default constructor
    ProductionWorker();
    //other constructor
    ProductionWorker(string employeeName, string employeeNumber, string hireDate, int shift, double hourlyPayRate);
    //destructor
    ~ProductionWorker();
    //copy constructor
    ProductionWorker(const ProductionWorker&);
    //setter member functions
    void setShift(int shift);
    void setHourlyPayRate(double hourlyPayRate);
    //getter member functions
    int getShift();
    double getHourlyPayRate();
    void output();
};
int main(int argc, char** argv) 
{

    //if you want the user to input the info
    //get the info for the production worker employee
    string employeeName;
    string employeeNumber;
    string hireDate;
    int shift = 0;
    double hourlyPayRate = 0.0;
    
    cout <<"Enter the name of the Employee: ";
    getline(cin, employeeName);
    cout <<"Enter the Employee's number: ";
    cin >> employeeNumber;
    cin.ignore();
    cout <<"Enter the date the Employee was hired: ";
    getline(cin, hireDate);
    cout << "Enter the shift (1 for day shift and 2 for night shift): ";
    cin >>shift;
    cout <<"Enter the Hourly Pay Rate for the employee: ";
    cin >>hourlyPayRate;
            
    //make an instance for the ProductionWorker class
    ProductionWorker worker(employeeName, employeeNumber, hireDate, shift, hourlyPayRate);
    
    //output the production worker employee information
    worker.output();
    return 0;
}

//Default constructor
Employee::Employee()
{
    employeeName = "";
    employeeNumber = "";
    hireDate = "";
}

//constructor 2.0
Employee::Employee(string employeeName, string employeeNumber, string hireDate)
{
    this->employeeName = employeeName;
    this->employeeNumber = employeeNumber;
    this->hireDate = hireDate;
}

//destructor
Employee::~Employee()
{
    
}

//copy constructor
Employee::Employee(const Employee& employee)
{
    this->employeeName = employee.employeeName;
    this->employeeNumber = employee.employeeNumber;
    this->hireDate = employee.hireDate;
}

//setter member functions
void Employee::setEmployeeName(string employeeName)
{
    this->employeeName = employeeName;
}

void Employee::setEmployeeNumber(string employeeNumber)
{
    this->employeeNumber = employeeNumber;
}

void Employee::setHireDate(string hireDate)
{
    this->hireDate = hireDate;
}

//getter member functions
string Employee::getEmployeeName()
{
    return employeeName;
}

string Employee::getEmployeeNumber()
{
    return employeeNumber;
}

string Employee::getHireDate()
{
    return hireDate;
}

void Employee::output()
{

    cout <<"Employee Name: "<<employeeName<<endl;
    cout<<"Employee Number: "<<employeeNumber<<endl;
    cout<<"Hire Date: "<<hireDate<<endl;
}

//default constructor
ProductionWorker::ProductionWorker()
        :Employee()
{
    shift = 0;
    hourlyPayRate = 0.0;
}

//constructor 2.0
ProductionWorker::ProductionWorker(string employeeName, string employeeNumber, string hireDate, int shift, double hourlyPayRate)
        :Employee(employeeName, employeeNumber, hireDate)
{
    this->shift = shift;
    this->hourlyPayRate = hourlyPayRate;
}

//destructor
ProductionWorker::~ProductionWorker()
{
    
}

//copy constructor
ProductionWorker::ProductionWorker(const ProductionWorker& productionWorker)
    :Employee(productionWorker)
{
    this->shift = productionWorker.shift;
    this->hourlyPayRate = productionWorker.hourlyPayRate;
}

//setter
void ProductionWorker::setShift(int shift)
{
    this->shift = shift;
}

void ProductionWorker::setHourlyPayRate(double hourlyPayRate)
{
    this->hourlyPayRate = hourlyPayRate;
}

//getter
int ProductionWorker::getShift()
{
    return shift;
}

double ProductionWorker::getHourlyPayRate()
{
    return hourlyPayRate;
}

void ProductionWorker::output()
{
    Employee::output();
    //switch the 1 and 2 to the correct cirresponding shifts for stylistic reasons
    switch(shift)
    {
        case 1:
            cout <<"Employee Shift: Day Shift"<<endl;
            break;
            
        case 2:
            cout <<"Employee Shift: Night Shift"<<endl;
            break;
    }
    cout<<"Hourly Pay Rate: $"<<fixed<<setprecision(2)<<hourlyPayRate<<endl;
}