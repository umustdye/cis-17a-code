
/* 
 * File:   Homework 2.cpp
 * Author: Heidi Dye
 *
 * Created on April 20, 2019, 7:21 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include <cstring>

using namespace std;
void one();
int* reversearray(int[], int);
void two();
double findaverage(int*, int);
int findmode(int*, int);
int findmedian(int*, int);
void three();
int stringlength(char*);
void four();
void reversestring(char*, int);
void five();
void arranger(char[], char[], char[]);
void six();
void sumofdigits(char[], int);

int main(int argc, char** argv) {

   one();
   two();
   three();
   four();
   five();
   six();
   
    return 0;
}

void one()
{
   //page 555
   cout<< "#1: Reverse Array"<<endl;
   
   //get the size from the user
   cout<< "Enter the size of your array: ";
   int size = 0;
   cin>>size;
   
   //initialize the original array from the user
   int array[size];
   
   //fill the array
   int input = 0;
   for(int i= 0; i<size; i++)
   {
       input = 0;
       cout<<"Enter array element #"<<i+1<<": ";
       cin >>input;
       array[i] = input;
   }
   
   //output the original array before reversing
   cout<<"Original Array: ";
   for (int i=0; i<size; i++)
   {
       cout<<array[i]<<" ";
   }
   cout<<endl;
   
   
   //this will be the pointer that points to the reversedarray pointer array 
   //from reversearray.
   int* pointtoreversedarray;
   pointtoreversedarray = reversearray(array, size);


   //output the reversed array from the pointtoreversedarray.
   cout<<"Reversed Array: ";
   for (int i = 0; i<size; i++)
   {
       cout<<*(pointtoreversedarray + i)<<" ";
   }
   cout<<endl;
   
   //delete the pointtoreversedarray
   delete pointtoreversedarray;
   pointtoreversedarray = NULL;
}


int* reversearray(int array[], int size)
{
    //make the array that will be reversed. I had to make it pointer, 
    //no other ways worked.
    int* reversedarray = new int[size];
    
    //copy and reverse the array elements into reversedarray
    for(int i = 0; i<size; i++)
    {
        //reversedarray starts form the end, while array starts from
        //the beginning. This switches the places of the array elements... hence 
        //the reversing.
        reversedarray[size-(i+1)]= array[i];
    }
 
    //return the reversedarray pointer
    return reversedarray;
}

void two()
{
    //Page 555
    cout << endl;
    cout << "#2: Movie Statistics" << endl;
    
    //Ask the User how many students were surveyed
    int studentamount = 0;
    cout << "How many students were surveyed?: ";
    cin >> studentamount;
    
    //create the dynamic array to hold the movie statistics
    int* howmanymoviesstats = new int[studentamount];
    
    //fill the array with data
    int input = 0;
    for(int i = 0; i < studentamount; i++)
    {
        //make sure that input always has a blank slate 
        input = 0;
        cout << "How many movies did student #" << (i+1) << " watch?: ";
        cin >> input;
        
        //input validation, must not be negative
        if(input < 0)
        {
           cout << "ERROR! Input cannot be negative...." << endl;
           exit(0);
        }
        
        //assign input to an element in the array
        howmanymoviesstats[i] = input;
    }
    
    //find the average
    double average = findaverage(howmanymoviesstats, studentamount);
    
    //output the average
    cout << "Average: "<<average<<endl;
    
    //find the mode
    int mode = 0;
    cout << "Mode: ";
    
    mode = findmode(howmanymoviesstats, studentamount);
    if (mode == -1)
    {
        cout << "There is no mode."<<endl;
    }
    else
    {
        cout<<mode<<" ";
    }
    
    cout<<endl;
    
      
    //find median
    int median = findmedian(howmanymoviesstats, studentamount);
    cout<< "Median: "<<median<<endl;
    
}

double findaverage(int* array, int size)
{
    //declare a variable that tallies up all the elements in the array
    int totalsum = 0;
    
    //go through the array and tally up all the elements
    for(int i = 0; i < size; i++)
    {
        totalsum = totalsum + array[i];
    }
    
    //take the totalsum and divide by the size of the array
    double average = totalsum/size;
    
    //return the average to the function call
    return average;
}


int findmode(int* array, int size)
{
    //ifmode is for adding up if a certain element has a duplicate
    int ifmode = 0;
    
    //ifmodecompare is for the previous element that has a duplicate to compare 
    //against the next element has a duplicate
    int ifmodecompare = 0;
    
    //ismode is for the element that has the most duplicates
    int ismode = 0;
    
    //find the mode
    for(int i = 0; i < size; i++)
    {
        // reset the value of ifmode or the if mode will keep on accumulating
        //for all the elements
        ifmode = 0;
        
        
        for(int j = 0; j < size; j++)
        {
            
            //step through the array and test all elements for any duplicates
            if ((array[i] == array[j]) && !(i==j))
            {
                ifmode = ifmode + 1;
            }
            //testing which element has the most duplicates
            if (ifmode > ifmodecompare)
            {
                ismode = array[i];
                ifmodecompare = ifmode;
            }
        }
       
    }
   
    //if there were no repeating elements (requirement from the original question)
    if(ifmodecompare == 0)
    {
        ismode = -1;
    }
    
    return ismode;
}




int findmedian(int* array, int size)
{
    //sort the array in ascending order
    bool swap =false; 
    double temp  = 0;
              do
       {
           swap = false;
           for (int i=0; i<(size-1); i++)
           {
                   if (array[i] > array[i+1])
                   {
                       temp = array[i];
                       array[i]= array[i+1];
                       array[i+1]=temp;
                       swap =true;
                   }
           }
       }while(swap); 
       
       //find the value in the center of the sorted array
       int median = 0;
       
       //if the array has an even amount of elements
       if((median % 2)==1)
       {
           median = size/2;
           median = array[median+1];
       }
       
       //if the array has an odd amount of elements
       if(median%2==0)
       {
           median = size/2;
           median = array[median];
       }
       return median;
}


void three()
{
    //page 607
    cout << endl;
    cout << "#3: String Length" << endl;
    char* input= new char[100];
    //have the user enter their c-string
    cout << "Enter your string: ";
    cin>>input;
    //use the stringlength function to find the number of elements in the c-string
    //output the string length
    cout << "String Length: "<<stringlength(input)<<endl;
    //delete the array
    delete[]input;
    input = NULL;
            
}
int stringlength(char* input)
{
    int stringlength = 0;
    // go throught cstring array and find the null terminator, aka the last character
    for (;input[stringlength] != '\0' ; stringlength++)
    {
    }
    return stringlength;
}

void four()
{
    //page 607
    cout<<"#4: Backward String"<<endl;
    //create a character array
    char* input = new char[100];
    //have the user enter in a string
    cout <<"Enter your string: ";
    cin >>input;
    //use the stringlength function from #3 to find the length of the string
    int size = stringlength(input);
    //send the array/string and the size of it to the reverse array function
    reversestring(input, size);
    //delete the array
    delete[]input;
    input = NULL;
}

void reversestring(char* input, int size)
{
    //create a new array to copy in the reversed array into
    char* reversedinput = new char[100];
    //go through both of the arrays and assign elements to the new array from
    //the last element
    for (int i = 0; i<size; i++)
    {
        reversedinput[size- (i+1)]= input[i];
    }
    //output the reversed array
    for (int i = 0; i<size; i++)
    {
        cout<<reversedinput[i];
    }
    cout<<endl;
    //delete the reversed array
    delete[]reversedinput;
    reversedinput = NULL;
}

void five()
{
    //page 608
    cout<<endl<<"#5: Name Arranger"<<endl;
    //create three array for the first, middle, and last name
    char first[100];
    char middle[100];
    char last[100];
    //get the name from the user and assign it to the appropriate array
    cout <<"Enter your first name: ";
    cin >> first;
    cout << "Enter your middle name: ";
    cin >> middle;
    cout << "Enter you last name: ";
    cin >> last;
    //send all three of the arrays to the arranger function
    //Last, First Middle
    arranger(first, middle, last);
}

void arranger(char first[], char middle[], char last[])
{
    //use the stringlength function from #3 to fin the length of each of the arrays
    int firstsize = stringlength(first);
    int middlesize = stringlength(middle);
    int lastsize = stringlength(last);
    //for the fourth array, the size is all the name sizes plus 3 for the two spaces
    //and one comma
    int stringsize = 3 + firstsize + middlesize + lastsize;
    //fourth array for the new arranged array "Last, First Middle"
    string array[stringsize];
    // put the last name first
    for(int i = 0; i<lastsize; i++)
    {
        array[i]= last[i];
    }
    //add the comma and a space after the fist name
    array[lastsize] = ",";
    array[lastsize + 1] = " ";
    //insert the first name after the space
    for (int j =0; j<firstsize; j++)
    {
        array[lastsize+ 2 +j] = first[j];
        
    }
    //insert a space after the first name
    array[lastsize+2+firstsize] = " ";
    
    //i = middlesize + lastsize +1;
    //insert the middle name after the space
    for(int j = 0; j<middlesize; j++)
    {
        
        array[firstsize+ lastsize +3 +j]= middle[j];
       
    }
    //output the new arranged array
    for(int j = 0 ; j<stringsize; j++ )
    {
        cout<<array[j];
    }
    cout<<endl;
}


void six()
{
    //page 608
    cout<<"#6: Sum of Digits in String"<<endl;
    //create the cstring
    char input[100];
    //get the list of numbers from the user
    cout <<"Enter  a series of single-digit numbers with no spaces: ";
    cin >> input;
    //get the size of the cstring form the stringlength function form #3
    int size = stringlength(input);
    //add all the numbers from the string together
    sumofdigits(input, size);
    //get the max and min digit
    int max  = input[0]-'0';
    int min = input[0]-'0';
    for (int i = 0; i<size; i++)
    {
        
        if(max <(input[i])-'0')
        {
            max = input[i] - '0';
        }
        if (min>(input[i]-'0'))
        {
            min = input[i]-'0';
        }
    }
    //output the min and max
    cout<<"Max: "<<max<<endl;
    cout<<"Min: "<<min<<endl;
    
}

void sumofdigits(char input[], int size)
{
    int sum =0;
    //go through each element in the array and add it together in sum
    for (int i = 0; i< size; i++)
    {
        sum = sum + input[i] - '0';
    }
    //output the sum
    cout<<"Sum: "<<sum<<endl;
   
    
}