

/* 
 * File:   Homework 5.cpp
 * Author: Heidi Dye
 *
 * Created on April 30, 2019, 12:19 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include <cstring>

using namespace std;
//classes
//#1:Number Array Class
class NumberArray
{
private:
    float* floatPointArray;
    int sizeOfArray;
public:
    //default constructor
    NumberArray();
    //constructor
    NumberArray(int sizeOfArray);
    //destructor: delete the floatPointArray
    ~NumberArray();
    //member functions
    //store a number in any element of the array
    void storeNumber();
    //retrieve a number from any element of the array
    void retrieveNumber();
    //return the highest value stored in the array
    float highestValue();
    //return the lowest value stored in the array
    float lowestValue();
    //return the average of all the numbers stored in the array
    float average();
};
//#2: Coin Toss Simulator
class Coin
{
private:
    //holds "heads" or "tails" indication the side of the coin that is facing up
    string sideUp;
public:
    //default constructor that RANDOMLY determines the side of the coin is 
    //facing up, and initializes that sideUp member variable accordingly
    Coin();
    //simulate a tossed coin and sets the sideUp member accordingly
    void toss();
    //return the value of sideUp
    string getSideUp();
};

//#3: Freezing and Boiling Points
class FreezeOrBoil
{
private:
    int temperature;
public:
    //default constructor
    FreezeOrBoil();
    //constructor
    FreezeOrBoil(int temperature);
    //getter function
    int getTemperature();
    //setter
    void setTemperature(int temperature);
    bool isEthylFreezing();
    bool isEthylBoiling();
    bool isOxygenFreezing();
    bool isOxygenBoiling();
    bool isWaterFreezing();
    bool isWaterBoiling();
};
//#4 RetailItem Class
class RetailItem
{
private:
    string description;
    int unitsOnHand;
    double price;
public:
    RetailItem();
    RetailItem(string description, int unitsOnHand, double price);
    //getters
    string getDescription();
    int getUnitsOnHand();
    double getPrice();
};

//#4 Inventory class 
class Inventory
{
private:
    //item's item number
    int itemNumber;
    //quantity of items on hand
    int quantity;
    //wholesale per-unit cost of the item
    double cost;
    //total inventory cost of the item (quantity * cost)
    double totalCost;
    //Retail object
    //RetailItem item1;
    //RetailItem item2;
    //RetailItem item3;
public:
    //default constructor
    Inventory ();
    //constructor #2
    Inventory(int itemNumber, int quantity, double cost);
    //setter member functions
    void setItemNumber(int itemNumer);
    void setQuantity(int quantity);
    void setCost(double cost);
    double setTotalCost();
    //getter member functions
    int getItemNumber();
    int getQuantity();
    double getCost();
    double getTotalCost();
        
};

//#3 Cash Register class
class CashRegister
{
private:
    //make an inventory class object
    Inventory inventory;
    double itemCost;
    double unitPrice;
    double purchaseSubTotal;
    double salesTax;
    double purchaseTotal;
    
public:
    //default constructor
    CashRegister();
    void getPurchaseFromUser();
    void getItemCost();
    double getUnitPrice();
    double getPurchaseSubTotal();
    double getPurchaseTotal();
    int getOnHand();
    double getSalesTax();
    
    
};

//functions
void one();
void two();
void three();
void four();

int main(int argc, char** argv) {
    
    //one();
    //two();
    //three();
    four();
    return 0;
}

void one()
{
    //page 812
    cout<<"#1: Number Array Class"<<endl;
    //get the size from the user
    cout<<"What size would you like your float point array to be: ";
    int sizeOfArray;
    cin >> sizeOfArray;
    //make a object for the NumberArray Class
    NumberArray numberArray (sizeOfArray);
    //if the menu option is "-1" or not
    bool promptContinuer = true;
    bool inputContinuer = true;
    bool receiveContinuer = true;
    //menu option
    int inputPrompt = 0;
    int inputInput = 0;
    int inputReceive = 0;
    //returning the values from the class member functions
    float average = 0.0;
    float lowestValue = 0.0;
    float highestValue = 0.0;
    do
    {
        inputInput = 0;
        
        do
        {
            inputReceive = 0;
            numberArray.storeNumber();
            highestValue = numberArray.highestValue();
            lowestValue = numberArray.lowestValue();
            average = numberArray.average();
            cout <<"Highest Value: "<<highestValue<<endl;
            cout <<"Lowest Value: "<<lowestValue<<endl;
            cout <<"Average: "<<average<<endl;
            do
            {
                cout<<"Would you like to retrieve a value from the array (Enter -1 for no, and any other number for yes): ";
                cin >> inputReceive;

                if(inputReceive == -1)
                {
                    receiveContinuer = false;
                }
                else
                {
                    numberArray.retrieveNumber();
                }
                    
                
            }while(receiveContinuer == true);
            
            cout <<" Would you like to enter another value into your array? (Enter -1 for no, and any other number for yes): ";
            cin >> inputInput;
            if(inputInput == -1)
            {
            inputContinuer = false;
            }
        }while (inputContinuer == true);
         
        cout << "Would you like to store or retrieve another value (Enter -1 for no, and any other number for yes): ";
        cin >> inputPrompt;
        if(inputPrompt == -1)
        {
            promptContinuer = false;
        }
            
        
    }while(promptContinuer==true);
    
}

//default constructor
NumberArray::NumberArray()
{
    sizeOfArray = 0;
    floatPointArray = new float[sizeOfArray];
}

//main constructor
NumberArray::NumberArray(int sizeOfArray)
{
    this->sizeOfArray = sizeOfArray;
    floatPointArray = new float[sizeOfArray];
    //fill the array with zeros to begin with
    //we don't want junk values
    for (int i=0; i<sizeOfArray; i++)
    {
        floatPointArray[i]= 0.0;
    }
}

//destructor
NumberArray::~NumberArray()
{
    delete[]floatPointArray;
    floatPointArray = NULL;
}
    
//member functions for NumberArray class
//store a number in any element of the array
void NumberArray::storeNumber()
{
    //ask the user which element they would like to change
    cout << "In which element, from 0 to "<<sizeOfArray-1<<" would you like to store a new value in: ";
    int elementNumber = 0;
    cin >> elementNumber;
    //ask the user for the value they want in that element
    cout << "What would you like to store in array element "<<elementNumber<<" : ";
    float newElement = 0.0;
    cin >> newElement;
    //put that value in the array in the desired element
    floatPointArray[elementNumber] = newElement;
}
//retrieve a number from any element of the array
void NumberArray::retrieveNumber()
{
    //ask the user which element number they would like to retrieve
    cout << "Which element number would you like to retrieve: ";
    int elementNumber = 0;
    cin >> elementNumber;
    cout << "At array element "<<elementNumber<<", this value was retrieved: "<<
            floatPointArray[elementNumber]<<endl;
}
//return the highest value stored in the array
float NumberArray::highestValue()
{
    float highestValue = floatPointArray[0];
    for (int i=0; i< sizeOfArray; i++)
    {
        if(highestValue < floatPointArray[i] )
        {
            highestValue= floatPointArray[i];
        }
    }
    
    return highestValue;
}
//return the lowest value stored in the array
float NumberArray::lowestValue()
{
    float lowestValue = floatPointArray[0];
    for (int i=0; i< sizeOfArray; i++)
    {
        if(lowestValue > floatPointArray[i] )
        {
            lowestValue= floatPointArray[i];
        }
    }
    
    return lowestValue; 
}
//return the average of all the numbers stored in the array
float NumberArray::average()
{
    float average;
    for (int i = 0; i<sizeOfArray; i++)
    {
        average = average + floatPointArray[i];
    }
    average = average/sizeOfArray;
    return average;
}

void two()
{
    //pg 812
    cout<<"#2: Coin Toss Simulator"<<endl;
    //seed the random number generator
    srand(static_cast<unsigned int>(time(0)));
    //make an instance for the Coin class
    Coin coin;
    //output the first coin toss
    cout<<"First toss: "<<coin.getSideUp()<<endl;
    //keep count how many "Tails" and "Heads" were tossed
    int headCount = 0;
    int tailCount = 0;
    //toss the coin 20 times
    for (int i = 0; i<20; i++)
    {
        //seed the random number generator
        srand(static_cast<unsigned int>(time(0)));
        //toss the coin
        coin.toss();
        //output the toss
        cout<<"Toss #"<<i+1<<": "<<coin.getSideUp()<<endl;
        //keep count of how many times "heads" or "tails" is tossed
        //compare the toss to either "heads" or "tails"

        
        if (coin.getSideUp() == ("Heads"))
        {
            headCount += 1;
        }
       
        if (coin.getSideUp() == ("Tails"))
        {
            tailCount+=1;
        }
        
    }
    //output total head count and total tail count
    cout<<"Total Head Count: "<<headCount<<endl;
    cout<<"Total Tail Count: "<<tailCount<<endl;
    
}

//default constructor that RANDOMLY determines the side of the coin is 
//facing up, and initializes that sideUp member variable accordingly
Coin::Coin()
{
    //int that will randomly generate the coin toss
    int toss = 0;
    //randomly generate "0" or "1"
    toss = rand()%2+1;
    //switch the "0" to "heads" and "1" to "tails"
    switch (toss)
    {
        case 1:
            this->sideUp = "Heads";
            break;
   
        case 2:
            this->sideUp = "Tails";
            break;  
    }
    
}

//simulate a tossed coin and sets the sideUp member accordingly
void Coin::toss()
{
    //seed the random number generator
    srand(static_cast<unsigned int>(time(0)));
    //int that will randomly generate the coin toss
    int toss = 0;
    //randomly generate "0" or "1"
    toss = 1+ rand()%2;
    //switch the "0" to "heads" and "1" to "tails"
    switch (toss)
    {
        case 1:
            sideUp = "Heads";
            break;
   
        case 2:
            sideUp = "Tails";
            break;  
    }
    this->sideUp = sideUp;
}

//return the sideUp
string Coin::getSideUp()
{
    return sideUp;
}

void three()
{
    //pg 814
    cout<<"#3: Freezing and Boiling Points"<<endl;
    //get the temp from the user
    int temp = 0;
    cout<<"Enter the temperature: ";
    cin>>temp;
    //create an object for the FreezeOrBoil class
    FreezeOrBoil fOB(temp);
    cout<<"At "<<temp<<": "<<endl;
    //see if the temp is boiling or freezing at any of the substances temp
    //ethyl
    if(fOB.isEthylFreezing() == true)
    {
        cout<< "Ethyl will freeze "<<endl;
    }
    if(fOB.isEthylBoiling() == true)
    {
        cout<< "Ethyl will boil"<<endl;
    }
    //oxygen
    if(fOB.isOxygenFreezing() == true)
    {
        cout<< "Oxygen will freeze "<<endl;
    }
    if(fOB.isOxygenBoiling() == true)
    {
        cout<< "Oxygen will boil"<<endl;
    }    
    //water
    if(fOB.isWaterFreezing() == true)
    {
        cout<< "Water will freeze "<<endl;
    }
    if(fOB.isWaterBoiling() == true)
    {
        cout<< "Water will boil"<<endl;
    }
    
}
//default constructor
FreezeOrBoil::FreezeOrBoil()
{
    //set temperature to zero
    temperature = 0;
}

//constructor
FreezeOrBoil::FreezeOrBoil(int temperature)
{
    this->temperature = temperature;
}

//getter function
int FreezeOrBoil::getTemperature()
{
    return temperature;
}

//setter function
void FreezeOrBoil::setTemperature(int temperature)
{
    this->temperature = temperature;
}

//other required functions... bleh
bool FreezeOrBoil::isEthylFreezing()
{
    bool ifFreezing = false;
    if(this->temperature < (-172))
    {
        ifFreezing  = true;
    }
    return ifFreezing;
}

bool FreezeOrBoil::isEthylBoiling()
{
    bool ifBoiling = false;
    if(this->temperature > (171))
    {
        ifBoiling = true;
    }
    return ifBoiling;
}

bool FreezeOrBoil::isOxygenFreezing()
{
    bool ifFreezing = false;
    if(this->temperature < (-361))
    {
        ifFreezing  = true;
    }
    return ifFreezing;
}

bool FreezeOrBoil::isOxygenBoiling()
{
    bool ifBoiling = false;
    if(this->temperature > (-305))
    {
        ifBoiling = true;
    }
    return ifBoiling;
}

bool FreezeOrBoil::isWaterFreezing()
{
    bool ifFreezing = false;
    if(this->temperature < (31))
    {
        ifFreezing  = true;
    }
    return ifFreezing;
}
bool FreezeOrBoil::isWaterBoiling()
{
    bool ifBoiling = false;
    if(this->temperature > (212))
    {
        ifBoiling = true;
    }
    return ifBoiling;
}

void four()
{
    //pages 814-815
    cout<<"#4: Cash Register"<<endl;
    
    CashRegister purchase;
    cout<<"Purchase subtotal: "<<purchase.getPurchaseSubTotal()<<endl;
    cout<<"Tax: "<<purchase.getSalesTax()<<endl;
    cout<<"Total: $"<<purchase.getPurchaseTotal();


}

RetailItem::RetailItem(string description, int unitsOnHand, double price)
{
    this->description = description;
    this-> unitsOnHand = unitsOnHand;
    this->price = price;
}

string RetailItem::getDescription()
{
    return description;
}

int RetailItem::getUnitsOnHand()
{
    return unitsOnHand;
}

double RetailItem:: getPrice()
{
    return price;
}
    

//default constructor
Inventory::Inventory()
{
    //RetailItem item1 ("Jacket", 12, 59.95);
    //RetailItem item2 ("Designer Jeans", 40, 34.95);
    //RetailItem item3 ("Shirt", 20, 24.95);
    itemNumber = 0;
    quantity = 0;
    cost = 0;
    totalCost = 0;
}
//2nd constructor
Inventory::Inventory(int itemNumber, int quantity, double cost)
{
    //item1.RetailItem("Jacket", 12, 59.95);
    //item2.RetailItem("Designer Jeans", 40, 34.95);
    //item3.RetailItem("Shirt", 20, 24.95);
    this->itemNumber = itemNumber;
    this->quantity = quantity;
    this->cost = cost;
    setTotalCost();
}

//setter member functions
void Inventory::setItemNumber(int itemNumber)
{
    this->itemNumber = itemNumber;
}
void Inventory::setQuantity(int quantity)
{
    this->quantity = quantity;
}
void Inventory::setCost(double cost)
{
    this->cost = cost;
}
double Inventory::setTotalCost()
{
    this->totalCost = cost*quantity;
}

//getter member functions
int Inventory::getItemNumber()
{
    return itemNumber;
}
int Inventory::getQuantity()
{
    return quantity;
}
double Inventory::getCost()
{
    return cost;
}
double Inventory::getTotalCost()
{
    return totalCost;
}

//default constructor
CashRegister::CashRegister()
{
    inventory.Inventory(0,0,0.0);
    itemCost = 0;
    unitPrice = 0;
    purchaseSubTotal = 0;
    salesTax = 0;
    purchaseTotal = 0;
}

//get the quantity and the item number
void CashRegister::getPurchaseFromUser()
{
    int itemNum;
    cout << "Enter the item number: ";
    cin >> itemNum;
    inventory.setItemNumber(itemNum);
    int quantity;
    cout << "Enter the quantity being purchased: ";
    cin >> quantity;
    //input validation
    if((quantity<0))
    {
        cout<<"Error! Input must be greater than zero..."<<endl;
        exit(1);
    }
    inventory.setQuantity(quantity);
    cout << "Enter the cost: ";
    double cost;
    cin >> cost; 
    inventory.setCost(cost);
}

//get the item's cost
void CashRegister::getItemCost()
{
    this->itemCost = inventory.getCost();
}
double CashRegister::getUnitPrice()
{
 
    unitPrice = itemCost * .3;
    unitPrice = this->unitPrice + itemCost;
    return unitPrice;
}

double CashRegister::getPurchaseSubTotal()
{
    purchaseSubTotal = inventory.getQuantity() * unitPrice;
    return purchaseSubTotal;
}

double CashRegister::getPurchaseTotal()
{
    salesTax = purchaseSubTotal * .06;
    purchaseTotal = purchaseSubTotal + salesTax;
}

double CashRegister::getSalesTax()
{
    return salesTax;
}