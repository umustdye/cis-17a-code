/* 
 * File:   Homework 6 #2.cpp
 * Author: Heidi Dye
 *
 * Created on May 8, 2019, 8:21 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

class FeetInches;

ostream &operator << (ostream &, const FeetInches&);

//classes
class FeetInches
{
private:
    //to hold number of feet
    int feet;
    //to hold number of inches
    int inches;
    void simplify();
public:
    //default constructor
    FeetInches();
    //copy constructor
    FeetInches(const FeetInches&);
    //destructor
    ~FeetInches();
    //Assignment overload
    FeetInches operator=(const FeetInches&);
    //setter member functions
    void setFeet(int feet);
    void setInches(int inches);
    //getter member functions
    int getFeet();
    int getInches();
    //operator overload
    bool operator<=(const FeetInches&);
    bool operator>=(const FeetInches&);
    bool operator!=(const FeetInches&);
    friend ostream &operator<<(ostream&, const FeetInches&);
};

int main(int argc, char** argv) {

    int feet;
    int inches;
     //call the first constructor
    FeetInches f1;
    //get the first set of values (feet and inches) from the user
    cout <<"Enter the feet for the first object: ";
    cin>>feet;
    cout << "Enter the inches for the first object: ";
    cin>>inches;
    //set the values
    f1.setInches(inches);
    f1.setFeet(feet);
        
    //call the second constructor
    FeetInches f2;
    //get the second set of values (feet and inches) from the user
    cout <<"Enter the feet for the second object: ";
    cin>>feet;
    cout << "Enter the inches for the second object: ";
    cin>>inches;
    //set the values
    f2.setInches(inches);
    f2.setFeet(feet);
    
    //compare the values
    if (f1>=f2)
    {
        cout << "F1 is greater than or equal to F2"<<endl;
    }
    if (f1<=f2)
    {
        cout << "F1 is less than or equal to F2"<<endl;
    }
    if (f1 != f2)
    {
        cout << "F1 is not equal to F2"<<endl;
    }
    return 0;
}

//constructor
FeetInches::FeetInches()
{
    feet = 0;
    inches = 0;
    simplify();
}

//destructor
FeetInches::~FeetInches()
{
    
}

//copy constructor
FeetInches::FeetInches(const FeetInches& feetInches)
{
    this->feet = feetInches.feet;
    this->inches = feetInches.inches;
    simplify();
}

//assignment overload
FeetInches FeetInches::operator =(const FeetInches& rhs)
{
    //self argument check
    if (this == &rhs)
    {
        return *this;
    }
    
    //copy data
    this->feet = rhs.feet;
    this->inches = rhs.inches;
    return *this;
}

//setter member functions
void FeetInches::setFeet(int feet)
{
    this->feet = feet;
}
void FeetInches::setInches(int inches)
{
    this->inches = inches;
}

//getter member functions
int FeetInches::getFeet()
{
    return feet;
}
int FeetInches::getInches()
{
    return inches;
}

void FeetInches::simplify()
{
    if(inches >= 12)
    {
        feet += (inches / 12);
        inches = inches % 12;
    }
    
    else if(inches < 0)
    {
        feet -= ((abs(inches) / 12) + 1);
        inches = 12 - (abs(inches) % 12);
    }
}

bool FeetInches::operator >=(const FeetInches& rhs)
{
    bool status;
    if(feet>=rhs.feet)
    {
        status = true;
    }
    else if(feet==rhs.feet && inches>=rhs.inches)
    {
        status = true;
    }
    else
    {
        status = false;
    }
    return status;
}

bool FeetInches::operator <=(const FeetInches& rhs)
{
    bool status;
    if(feet<=rhs.feet)
    {
        status = true;
    }
    else if(feet==rhs.feet && inches<=rhs.inches)
    {
        status = true;
    }
    else
    {
        status = false;
    }
    return status;
}

bool FeetInches::operator !=(const FeetInches& rhs)
{
    bool status;
    if(!(feet==rhs.feet && inches==rhs.inches))
    {
        status = true;
    }
    else
    {
        status = false;
    }
    return status;
}

ostream& operator<<(ostream& out, const FeetInches& feetInches)
{
    out<<feetInches.feet<<endl;
    cout<<feetInches.inches<<endl;
    return out;
}