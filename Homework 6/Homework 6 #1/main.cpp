

/* 
 * File:   Homework 6 #1.cpp
 * Author: Heidi2
 *
 * Created on May 6, 2019, 3:56 PM
 */

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <vector>

using namespace std;
//enum
enum MonthDays 
{
    JAN = 31, FEB = 59, MAR = 90, APR = 120, MAY = 151, JUN = 181, JUL = 212, AUG = 243, SEP = 273,
    OCT = 304, NOV = 334, DEC = 366 
};
/*enum MonthDays
{
    JAN = 0, FEB = 31, MAR = 59, APR = 90, MAY = 120, JUN = 151, JUL = 181, AUG = 212, SEP = 243,
    OCT = 273, NOV = 304, DEC = 334
};*/

std::vector<MonthDays> build_all_MonthDays()
{
    const MonthDays all[] = {JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC};
    
    return std::vector<MonthDays>(all, all + sizeof(all) / sizeof(MonthDays));
}

std::vector<MonthDays> all_MonthDays = build_all_MonthDays();

//classes
class DayofYear
{
private:
    //0-365 day of the year from user
    int dayOfYear;
    //month index from output member function
    int monthNumber;
    //store array element for months
    int monthCounter;
    //month name 
    string month;
    //represents a day of the month
    int dayOfMonth;
public:
    //contains month names
    static const string monthConverter[];
    //contains month length
    static const int monthLength[];
    //default constructor
    DayofYear();
    // 1st constructor
    DayofYear(int dayOfYear);
    //2nd constructor
    DayofYear(string month, int dayOfMonth);
    //destructor
    ~DayofYear();
    //copy constructor
    DayofYear(const DayofYear&);
    //output function
    void print();
    //prefix ++
    DayofYear operator++();
    //post-fix ++
    DayofYear operator++(int);
    //prefix --
    DayofYear operator--();
    //postfix --
    DayofYear operator--(int);
    void helpIncrement();
    void helpDecrement();
    //getter functions
    string getMonth();
    int getDayOfMonth();
};
//fill our month array with the month names
const string DayofYear::monthConverter[] = {"January", "February", "March", "April", "May", "June", 
                            "July", "August", "September", "October", 
                            "November", "December"};
const int DayofYear::monthLength[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

int main(int argc, char** argv) {
    cout << "Day of the Year"<<endl;
    // get the dayOfYear from the user
    int dayOfYear = 0;
    cout<<"Enter the day of the year: ";
    cin>>dayOfYear;
    //make an object for the DayofYear class
    DayofYear dOY(dayOfYear);
    dOY.print();
    //part two....
    cout<<endl<<"Day of Year Modification"<<endl;
    //get the month name and month day from the user
    string month;
    int dayOfMonth;
    cout<<"Enter the month (January, February, ....., December): ";
    cin>>month;
    cout<<"Enter the day of the month: ";
    cin>>dayOfMonth;
    //call our constructor
    DayofYear dOM(month, dayOfMonth);
    //demonstrate the post increment
    dOM++;
    //dOM.operator ++();
    cout <<"After Post Incrementing..."<<endl;
    cout <<dOM.getMonth()<<" "<<dOM.getDayOfMonth()<<endl;
    //demonstrate the pre increment
    cout <<"After Pre Incrementing..."<<endl;
    ++dOM;
    cout <<dOM.getMonth()<<" "<<dOM.getDayOfMonth()<<endl;
    //demonstrate the post decrement
    cout <<"After Post Decrementing..."<<endl;
    dOM--;
    cout <<dOM.getMonth()<<" "<<dOM.getDayOfMonth()<<endl;
    //demonstrate the pre decrement
    cout <<"After Pre Decrementing..."<<endl;
    --dOM;
    cout <<dOM.getMonth()<<" "<<dOM.getDayOfMonth()<<endl;
    
    return 0;
}

//default constructor
DayofYear::DayofYear()
{
    dayOfYear = 0;
}

//1st constructor from OG Day of Year program
DayofYear::DayofYear(int dayOfYear)
{
    this->dayOfYear = dayOfYear;
}

//destructor
DayofYear::~DayofYear()
{
    
}

//copy constructor
DayofYear::DayofYear(const DayofYear& dayOfYear)
{
    this->dayOfYear = dayOfYear.dayOfYear;
}

void DayofYear::print()
{
    //month is for our string month array to find the corresponding month name
    monthNumber = 0;
    int dayFromEnum = 0;
    //make an object for the enum MonthDays
    MonthDays md;
    for (std::vector<MonthDays>::const_iterator md = all_MonthDays.begin(); md != all_MonthDays.end(); ++md)
    {
        MonthDays monthD = *md;
        //compare our end of the month from the enum and compare it to the 
        //dayOfYear
        if ((monthD<dayOfYear))
        {
            //during our last loop, the value from the enum will be saved
            dayFromEnum = monthD;
            monthNumber = (monthNumber+ 1)%12;
        }
        //since the beginning of JAN is not included in the enum MonthDays
        if(dayOfYear <= 31)
        {
            dayFromEnum = 0;
            monthNumber = 0;
        }
        
        //for the beginning of the month
        if(dayOfYear == monthD)
        {
            dayFromEnum = dayOfYear - 1;
            monthNumber = (monthNumber+ 1)%12;
        }
    }
    //output the month name and day
    cout<<"Day "<<this->dayOfYear<<" would be "<<monthConverter[monthNumber]<<" "<<(dayOfYear - dayFromEnum)<<endl;
}

//2nd constructor
DayofYear::DayofYear(string month, int dayOfMonth)
{
    //check in the month
    for (int i = 0; i<12; i++)
    {
        if (month == monthConverter[i])
        {
            this->month = month;
            this->monthCounter = i;
        }
    }
    //cout<<month<<endl;
    //cout<<monthCounter<<endl;
    //error checking, a month cant be more than 31 days long, 
    //nor can it be negative
    //JAN, MAR, MAY, JUL, AUG, OCT, DEC
    if ((monthCounter == 0) || (monthCounter == 2) || (monthCounter == 4) || (monthCounter == 6))
    {
        if(dayOfMonth>0 && dayOfMonth<32)
        {
            this->dayOfMonth = dayOfMonth;   
        }
    }
       
    else if ((monthCounter == 7) || (monthCounter == 9) || (monthCounter == 11))
    {
        if(dayOfMonth>0 && dayOfMonth<32)
        {
            this->dayOfMonth = dayOfMonth;   
        }
    }
    //APRIL, JUN, SEP, NOV
    else if((monthCounter == 3)|| (monthCounter == 5) || (monthCounter == 8) || (monthCounter == 10))
    {
        if(dayOfMonth>0 && dayOfMonth<31)
        {
            this->dayOfMonth = dayOfMonth;   
        }
    }
    //FEB
    else if(monthCounter == 1)
    {
        if(dayOfMonth>0 && dayOfMonth<29)
        {
            this->dayOfMonth = dayOfMonth;   
        }
    }
    
    else
    {
        cout<<"Error! Number entered for day is outside of range."<<endl;
        exit(1);                
    }
            
}

//prefix ++
DayofYear DayofYear::operator ++()
{
    helpIncrement();
    ++ dayOfMonth;
    return *this;
}

//postfix ++
DayofYear DayofYear::operator ++(int)
{
    //copy constructor
    DayofYear temp(*this);
    helpIncrement();
    dayOfMonth++;
    return temp;
}

//prefix --
DayofYear DayofYear::operator --()
{
    helpDecrement();
    --dayOfMonth;
    return *this;
}

//postfix --
DayofYear DayofYear::operator --(int)
{
    //copy constructor
    DayofYear temp(*this);
    helpDecrement();
    dayOfMonth--;
    return temp;
}

void DayofYear::helpIncrement()
{
    if (month == "December" && dayOfMonth == 31)
    {
        monthCounter = 0;
        month = monthConverter[monthCounter];
        dayOfMonth = 0;
    }
    else if (dayOfMonth == monthLength[monthCounter])
    {
        month = monthConverter[++monthCounter];
        dayOfMonth = 0;
    }  
}

void DayofYear::helpDecrement()
{
    if (month == "January" && dayOfMonth == 1)
    {
        monthCounter = 11;
        month = monthConverter[monthCounter];
        dayOfMonth = 32;
    }
    else if (dayOfMonth == 1)
    {        
        month = monthConverter[--monthCounter];
        dayOfMonth = monthLength[monthCounter];
    }
}

string DayofYear::getMonth()
{
    return month;
}

int DayofYear::getDayOfMonth()
{
    return dayOfMonth;
}