

/* 
 * File:   practice with classes.cpp
 * Author: rcc
 *
 * Created on May 7, 2019, 7:42 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
//classes
class DogPark
{
private:
    string name;
    int age;
    int size;
    string* dogOwners;
public:
    //default constructor
    DogPark();
    //constructor
    DogPark(string name, int age, int size);
    //destructor
    ~DogPark();
    //copy constructor
    DogPark(const DogPark&);
    //assignment overload
    DogPark operator=(const DogPark&);
    DogPark operator+(const DogPark&);
    bool operator>=(const DogPark&);
};

int main(int argc, char** argv) {

    return 0;
}

//default constructor
DogPark::DogPark()
{
    name = "";
    age = 0;
    size = 0;
    dogOwners = new string[size];
}

//constructor
DogPark::DogPark(string name, int age, int size)
{
    this->name = name;
    this->age = age;
    this->size = size;
    dogOwners = new string[size];
    string ownerName;
    //fill the array
    for (int i = 0; i<this->size; i++)
    {
        cout<<"Enter the name of owner #"<<i+1<<": ";
        cin>>ownerName;
        dogOwners[i] = ownerName;
    }
}

//copy constructor
DogPark::DogPark(const DogPark& dogPark)
{
    this->name = dogPark.name;
    this->age = dogPark.age;
    this->size = dogPark.size;
    this->dogOwners = new string[this->size];
    
    //fill the array
    for (int i=0; i<this->size; i++)
    {
        this->dogOwners[i]= dogPark.dogOwners[i];
    }
}

//destructor
DogPark::~DogPark()
{
    delete[]dogOwners;
}

//assignment overload
DogPark DogPark::operator =(const DogPark& rhs)
{
    //self-assignment check
    if(this == &rhs)
    {
        return *this;
    }
    
    this->name = rhs.name;
    this->age = rhs.age;
    this->size = rhs.size;
    
    //delete existing dynamic data
    delete[]this->dogOwners;
    this->dogOwners = new string[this->size];
    //deep copy
    for(int i=0; i<this->size; i++)
    {
        this->dogOwners[i]= rhs.dogOwners[i];
    }
    
    return *this;
}

DogPark::operator +(const DogPark& rhs)
{
    int newSize = this->size + rhs.size;
    DogPark temp(newSize);
    //copy values
    for (int i=0; i<this->size; i++)
    {
        temp.dogOwners[i] = this->dogOwners[i];
    }
    for(int i = 0; i<this->size; i++)
    {
        temp.dogOwners[i+this->size] = rhs.dogOwners[i];
    }
    return temp;
}

bool DogPark::operator >=(const DogPark& rhs)
{
   
}