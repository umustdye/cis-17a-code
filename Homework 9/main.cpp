
/* 
 * File:   Homework 9.cpp
 * Author: Heidi Dye
 *
 * Created on May 28, 2019, 9:42 PM
 */


#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
//structs
struct Node
{
    int value;
    Node* next;
};
//classes
class LinkedList
{
private:
    Node* head;
public:
    LinkedList();
    ~LinkedList();
    void append(int val);
    void insert(int val);
    void remove(int val);
    bool search(int val);
    void print();
};
int main(int argc, char** argv) {

    //make an instance for the linkedList
    LinkedList linkedList;
    //append a value
    linkedList.append(5);
    //append a value
    linkedList.append(10);
    //append a value
    linkedList.append(20);
    //output the linkedList
    cout<<"After appending..."<<endl;
    linkedList.print();
    //insert 15
    linkedList.insert(15);
    //ouput the linkedList
    cout<<"After inserting..."<<endl;
    linkedList.print();
    //delete 10
    linkedList.remove(10);
    //output the linkedList
    cout<<"After deleting..."<<endl;
    linkedList.print();
    return 0;
}

//constructor
LinkedList::LinkedList()
{
    head = NULL;
}

//destructor
LinkedList::~LinkedList()
{
    //traversal pointer
    Node* nodePtr = head;
    
    while(nodePtr)
    {
        Node* temp = nodePtr;
        nodePtr = nodePtr->next;
        delete temp;
    }
    
    head = NULL;
}

//copy constructor


//append
void LinkedList::append(int val)
{
    Node* newNode = new Node;
    newNode->value = val;
    newNode->next = NULL;
    
    //if head does not point to anything
    if(!head)
    {
        head = newNode;
    }
    //find the end of the linklist and add the value to the end
    else
    {
        Node* nodePtr = head;
        while(nodePtr->next)
        {
        
            nodePtr = nodePtr->next;
        }
        nodePtr->next = newNode;
                
    }
}

void LinkedList::insert(int val)
{
    //create a brand new node
    Node* newNode = new Node;
    newNode->next = NULL;
    newNode->value = val;
    
    //check if the link list is empty
    //inserting at the beginning
    if(!head)
    {
        head = newNode;
    }
    
    //if inserting in the middle or at the end 
    else
    {
        //make previous and next pointers
        Node* next;
        Node* prev = NULL;
        //next starts at the beginning
        next = head;
        
        while(next != NULL && next->value < val)
        {
            prev = next;
            next = next->next;
        }
        //found the insert location
        if(prev == NULL)
        {
            newNode->next = head;
            head = newNode;
        }
        
        else
        {
            newNode->next = next;
            prev->next = newNode;
        }
    }
}

//deletes a node with a certain value
void LinkedList::remove(int val)
{
    //traversal pointer
    Node* nodePtr;
    //previous pointer
    Node* prev;
    
    //case: no linked list
    if(!head)
    {
        return;
    }
    
    //case: there is a linked list but delete at start
    if(head->value == val)
    {
        //move head to the next node
        nodePtr = head->next;
        //delete current head
        delete head;
        //make the new head the next value
        head = nodePtr;
    }
    
    else
    {
        //begin traversal of Linkedlist
        nodePtr = head;
        
        //Traverse until end or until the value is found
        while(nodePtr != NULL && nodePtr->value != val)
        {
            //traverse prev and current
            prev = nodePtr;
            //point to the next node
            nodePtr = nodePtr->next;
        }
        
        //deletion
        if(nodePtr)
        {
            prev->next = nodePtr->next;
            delete nodePtr;
        }
    }
}

bool LinkedList::search(int val)
{
    Node* nodePtr = head;
    while(nodePtr)
    {
        if(nodePtr->value == val)
        {
            return true;
        }
        //move to the next node
        nodePtr = nodePtr->next;
    }
    return false;
}

void LinkedList::print()
{
    //traversal
    Node* nodePtr = head;
    
    while(nodePtr)
    {
        cout << nodePtr->value << " ";
        //move to the next node
        nodePtr = nodePtr->next;
    }
    cout << endl;
}