
/* 
 * File:   Homework 1.cpp
 * Author: Heidi Dye
 *
 * Created on April 18, 2019, 11:14 AM
 */

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

void dynamic_array_creator();
void random_values_input(int*, int);
void dynamic_array_output(int*, int);
void one();
int random_element();
void two();
string* deleteEntry(string*, int&, int);
void three();
void coinToss();
void four();
void fillarray(double*, int);
void sortarray(double*, int);
void average (double*, int);
void five();
void deletemin(double*, int);
void six();
void kittycatproof(int);
void kittycatproof(double);
void kittycatproof (string);

int main(int argc, char** argv) {
    srand(static_cast<unsigned int>(time(0)));
    one();
    two();
    three();
    four();
    five();
    six();
    
    return 0;
}
void one()
{
    cout << "#1" << endl << endl;
    dynamic_array_creator();
    
}

void dynamic_array_creator()
{
   int size = 0; 
   cout << "How big do you want your dynamic array to be?" <<endl;
   cin >> size;
   kittycatproof(size);
   int* Darr = new int[size];
   random_values_input(Darr, size);
   
}

void random_values_input(int* Darr, int size)
{
   for (int i=0; i<size; i++)
   {
       Darr[i] = random_element();
   }
   
   dynamic_array_output(Darr, size);
}

int random_element()
{
    int random = rand()%100+1;
    return random; 
}

void dynamic_array_output(int* Darr,int size)
{
    for (int i=0; i<size; i++)
    {
        cout << Darr[i] << " ";
    }
    
    cout<<endl;
    
    delete[]Darr;
    Darr= NULL;
}

void two()
{   
    cout<<"#2"<<endl;
    int s = 5;
    int& size = s;
    int loc = 0;
    string* array = new string[size];
    //fill the array with strings
    cout << "Enter your Strings to fill the array " <<endl;
    for (int i = 0; i<size; i++)
    {
        string userinput;
        cout << "Element #" << i+1<<endl;
        cin >> userinput;
        array[i]=userinput;
    }
    
    loc = rand()%5;
    //loc = 3;
    //cout<<loc<<endl;
    
    int size2  = 4;
    string* newarray1 = new string [size2]; 
    newarray1 = deleteEntry(array, size, loc);
    //deleteEntry(array, size, loc);
    for(int i = 0; i<size; i++)
    {
        cout<<newarray1[i]<<" ";
    }
    
    cout<<endl;
    delete[]newarray1;
    newarray1 = NULL;
}

string* deleteEntry(string* array, int& size, int loc)
{

    //create the new array
    size= size -1;
    string* newarray = new string[size];
   
    for(int j=0; j<size; j++)
    {
        for (int i = 0; i<size; i++)
        {
            if (!(i==loc)) 
            {
                if (loc==0)
                {
                    newarray[0] = array[1];
                    newarray[1] = array[2];
                    newarray[2] = array[3];
                    newarray[3] = array[4];
                }
                if(loc==1)
                {
                    newarray[0] = array[0];
                    newarray[1] = array[2];
                    newarray[2] = array[3];
                    newarray[3] = array[4];
                }
                if(loc==2)
                {
                    newarray[0] = array[0];
                    newarray[1] = array[1];
                    newarray[2] = array[3];
                    newarray[3] = array[4];
                }
                if (loc == 3)
                {
                    newarray[0] = array[0];
                    newarray[1] = array[1];
                    newarray[2] = array[2];
                    newarray[3] = array[4];
                }
                if (loc == 4)
                {
                    newarray[0] = array[0];
                    newarray[1] = array[1];
                    newarray[2] = array[2];
                    newarray[3] = array[3];
                }
           
            }
      
        }
    }
    
    delete[]array;
    array = NULL;
    for(int i =0; i<size; i++)
    {
        cout<<newarray[i]<<" ";
    }
    cout<<endl;
    
    
    return newarray;
}

void three()
{
    cout<<"#3"<<endl;
    int input = 0;
    cout<<"How many times do you want to toss the coin?"<<endl;
    cin>>input;
    kittycatproof(input);
    int totalheads = 0;
    int totaltails = 0;
    int returnfunction = 0;
    for (int i = 0; i<input; i++)
    {
        coinToss();
    }
}

void coinToss()
{
    int toss = 0;
    toss = rand()%2+1;
    switch(toss)
    {
        case 1: cout<<"heads."<<endl;
        break;
        case 2: cout<<"tails."<<endl;
        break;
    }
    
}   

void four()
{
    cout <<"#4"<<endl;
    int size =0;
    cout<<"Enter the size for the array: ";
    cin>>size;
    kittycatproof(size);
    double* array = new double[size];
    fillarray(array, size);
    average(array, size);
    delete[]array;
    array= NULL;

}

void fillarray(double* array, int size)
{
    double input=0;
    for(int i = 0; i<size; i++)
    {
        cout<<"Enter Test Score #"<<i+1<<": ";
        cin>> input;
        if(input<0)
        {
            cout<<"ERORR! Test Score must be a positive value..."<<endl;
            exit(1);
        }
        kittycatproof(input);
        array[i] = input;
    }
    
    sortarray(array, size);
    
    
}    

void sortarray(double* array, int size)
{
    bool swap =false; 
    double temp  = 0;
              do
       {
           swap = false;
           for (int i=0; i<(size-1); i++)
           {
                   if (array[i] > array[i+1])
                   {
                       temp = array[i];
                       array[i]= array[i+1];
                       array[i+1]=temp;
                       swap =true;
                   }
           }
       }while(swap); 
       cout<<"Sorted Array: ";
       for(int i =0; i<size; i++)
       {
           cout<<array[i]<<" ";
       }
       cout<<endl;
    }

void average(double* array, int size)
{
    double average  = 0;
    
    for(int i = 0; i<size; i++)
    {
        average = array[i]+average;
    }
    average = average/size;
    cout<<"Average: "<<average<<endl;
        
}

void five()
{
    cout<<"#5"<<endl;
    int size =0;
    cout<<"Enter the size for the array: ";
    cin>>size;
    double* array = new double[size];
    fillarray(array, size);
    deletemin(array, size);

    delete[]array;
    array= NULL;
}

void deletemin(double* array, int size)
{
    //new array without min
    double* newarray = new double[size-1];
    for (int i=0; i<(size+1); i++)
    {
        newarray[i] = array[i+1];
    }
    
    cout<<"New array without the lowest test score: ";
    for (int i=0; i<size-1; i++)
    {
        cout<<newarray[i]<<" ";
    }
    cout<<endl;
    
    average(newarray, size-1);
    delete[] newarray;
    newarray = NULL;
}

void six()
{
    int size =0;
    
     cin.ignore();
    string filename;
    cout << "Enter name of the file you want to read: ";
    getline(cin,filename,'\n');     
    
    ifstream inFile;   
    
    inFile.open(filename.c_str()); 
    
    if (inFile)
    {
       
        inFile >> size;
            
       
        double* array = new double[size];

        
        for(int i=0;i<size;i++) 
        { 
            
                inFile >> array[i];
            
        }
        inFile.close();
        cout << "Read the file: " << filename << endl;
        sortarray(array, size);
        average(array, size);
        delete[]array;
        array= NULL;
        
       
        
    }
    
        
    else 
    {
        cout << "Could not read " <<filename << endl;
        exit(1);
    }
    
 
}

void kittycatproof(int input)
{
    bool valid;
    valid = input;
        if (!valid)
        {
            cin.clear ();
            cin.ignore (10000, '\n');
            cout<< "Sorry, invalid input. Please enter a numerical value\n";
            exit(1); 
        }
}
void kittycatproof(double input)
{
    bool valid;
    valid = input;
        if (!valid)
        {
            cin.clear ();
            cin.ignore (10000, '\n');
            cout<< "Sorry, invalid input. Please enter a numerical value\n";
            exit(1); 
        }
}