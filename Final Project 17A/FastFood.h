/************************************************************************
 *                                                                      *
 *                         Child class for Eat!                         *
 *                                                                      *
 ***********************************************************************/

/* 
 * File:   FastFood.h
 * Author: Heidi2
 *
 * Created on June 7, 2019, 4:04 PM
 */

#ifndef FASTFOOD_H
#define FASTFOOD_H

using namespace std;

class FastFood : public Eat
{
public:
    //default constructor
    FastFood();
    //constructor 
    FastFood(string nameOfPlace, double averagePrice, string typeOfDinningExperience, string typeOfFood, int numberOfoptionsToEatAt);
    //copy constructor
    FastFood(const FastFood& orig);
    //destructor
    virtual ~FastFood();
private:
    //displays the most popular menu items
    string* mostPopularMenuItems;
    //how many popular menu items there are
    int numberOfPopularMenuItems;
};

#endif /* FASTFOOD_H */

