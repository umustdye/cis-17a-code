/********************************************************************
 *                                                                  *
 *          PARENT CLASS FOR BOTH EAT AND VISTIT                    *
 *                                                                  *    
 ********************************************************************/


/* 
 * File:   PlacesToGo.h
 * Author: Heidi Dye
 *
 * Created on June 6, 2019, 11:26 AM
 */

#ifndef PLACESTOGO_H
#define PLACESTOGO_H
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;


class PlacesToGo {
public:
    //default constructor
    PlacesToGo();
    //constructor
    PlacesToGo(string nameOfPlace, double averagePrice, int howManyPeople);
    //copy constructor
    PlacesToGo(const PlacesToGo& orig);
    //destructor
    ~PlacesToGo();
    
    //assignment overload
    const PlacesToGo& operator=(const PlacesToGo&);
    
    //output overload pure virtual function
    //friend ofstream& operator<<(ofstream&, const PlacesToGo&);
    
    
    //setter member functions
    void setNameOfPlace(string nameOfPlace);
    void setAveragePrice(double averagePrice);
    void setHowManyPeople(int howManyPeople);
    
    //getter member functions
    string getNameOfPlace();
    double getAveragePrice();
    int getHowManyPeople();
    
    //output the name and price
    void output();
private:
    string nameOfPlace;
    double averagePrice;
    int howManyPeople;
};

#endif /* PLACESTOGO_H */

