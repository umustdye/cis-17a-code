/************************************************************************
 *                                                                      *
 *                     Child class for PlacesToGo!                      *
 *     Parent class for FineDineAndWine, FastFood, and CasualSitDown    *
 *                                                                      *
 ***********************************************************************/


// NOTE TO SELF: THIS CLASS CONTAINS A DYNAMIC ARRAY, REMEMBER TO DELETE...
 
/* 
 * File:   Eat.h
 * Author: Heidi2
 *
 * Created on June 7, 2019, 10:50 AM
 */

#ifndef EAT_H
#define EAT_H
#include "PlacesToGo.h"
#include <iostream>

using namespace std;


class Eat : public PlacesToGo 
{
public:
    //default constructor
    Eat();
    //constructor for just Eat variables
    Eat(string typeOfDinningExperience, string typeOfFood, int numberOfOptionsToEat);
    //constructor also assigns all of PlacesToGo's variables 
    Eat(string nameOfPlace, double averagePrice, string typeOfDinningExperience, string typeOfFood, int numberOfoptionsToEatAt);
    //copy constructor
    Eat(const Eat& orig);
    //destructor
    ~Eat();
    //assignment overload
    const Eat& operator=(const Eat&);
    //<< overload
    //friend ofstream& operator<<(ofstream&, const Eat&);
    //[] overload
    string &operator[](int indexNumber);
    //fill the optionsToEatAt array one spot at a time
    void fillOptionsToEatAt(int index);
    //output the values
    void output();
    //holds the various places to eat at 
    //nameOfPlace will chose the name from this array
    string* optionsToEatAt;
    //how many options there are to choose from
    int numberOfOptionsToEatAt;
private:
    //fast food, dinner theater, casual sit-down, fancy, etc
    string typeOfDinningExperience;
    //Mexican, Italian, Japanese, etc
    string typeOfFood;

};

#endif /* EAT_H */

