/************************************************************************
 *                                                                      *
 *                         Child class for Eat!                         *
 *                                                                      *
 ***********************************************************************/

/* 
 * File:   FineDineAndWine.h
 * Author: Heidi2
 *
 * Created on June 7, 2019, 5:15 PM
 */

#ifndef FINEDINEANDWINE_H
#define FINEDINEANDWINE_H

using namespace std;

class FineDineAndWine {
public:
    FineDineAndWine();
    FineDineAndWine(const FineDineAndWine& orig);
    virtual ~FineDineAndWine();
private:

};

#endif /* FINEDINEANDWINE_H */

