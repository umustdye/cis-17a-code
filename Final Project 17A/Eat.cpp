/************************************************************************
 *                                                                      *
 *                     Child class for PlacesToGo!                      *
 *     Parent class for FineDineAndWine, FastFood, and CasualSitDown    *
 *                                                                      *
 ***********************************************************************/

/*NOTE TO SELF: 
 * DELETE THE DYNAMIC ARRAY mostPopularMenuItems!
 */

/* 
 * File:   Eat.cpp
 * Author: Heidi2
 * 
 * Created on June 7, 2019, 10:50 AM
 */

#include "Eat.h"
#include "PlacesToGo.h"

Eat::Eat()
    :PlacesToGo()
{
    typeOfDinningExperience = " ";
    typeOfFood = " ";
    numberOfPopularMenuItems = 0;
    mostPopularMenuItems = new string[numberOfPopularMenuItems];
}

Eat::Eat(const Eat& orig)
    :PlacesToGo(orig)
{
    this->typeOfDinningExperience = orig.typeOfDinningExperience;
    this->typeOfFood = orig.typeOfFood;
    this->numberOfPopularMenuItems = orig.numberOfPopularMenuItems;
    this->mostPopularMenuItems = new string[this->numberOfPopularMenuItems];
    for (int i=0; i<this->numberOfPopularMenuItems; i++)
    {
        this->mostPopularMenuItems[i] = orig.mostPopularMenuItems[i];
    }
}

Eat::~Eat()
{
    delete[]mostPopularMenuItems;
    mostPopularMenuItems = NULL;
}

//constructor for just Eat variables
Eat::Eat(string typeOfDinningExperience, string typeOfFood, string* mostPopularMenuItems, int numberOfPopularMenuItems)
        :PlacesToGo()
{
    this->typeOfDinningExperience = typeOfDinningExperience;
    this->typeOfFood = typeOfFood;
    this->numberOfPopularMenuItems = numberOfPopularMenuItems;
    this->mostPopularMenuItems = new string[numberOfPopularMenuItems];
}

//constructor also assigns all of PlacesToGo's variables
Eat::Eat(string nameOfPlace, double averagePrice, string typeOfDinningExperience, string typeOfFood, string* mostPopularMenuItems, int numberOfPopularMenuItems)
        :PlacesToGo(nameOfPlace, averagePrice)
{
    this->typeOfDinningExperience = typeOfDinningExperience;
    this->typeOfFood = typeOfFood;
    this->numberOfPopularMenuItems = numberOfPopularMenuItems;
    this->mostPopularMenuItems = new string[numberOfPopularMenuItems];
}
