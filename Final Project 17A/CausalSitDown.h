/************************************************************************
 *                                                                      *
 *                         Child class for Eat!                         *
 *                                                                      *
 ***********************************************************************/

/* 
 * File:   CausalSitDown.h
 * Author: Heidi2
 *
 * Created on June 7, 2019, 5:14 PM
 */

#ifndef CAUSALSITDOWN_H
#define CAUSALSITDOWN_H

using namespace std;

class CausalSitDown {
public:
    CausalSitDown();
    CausalSitDown(const CausalSitDown& orig);
    virtual ~CausalSitDown();
private:

};

#endif /* CAUSALSITDOWN_H */

