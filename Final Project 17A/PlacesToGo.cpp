/********************************************************************
 *                                                                  *
 *          PARENT CLASS FOR BOTH EAT AND VISIT                    *
 *                                                                  *    
 ********************************************************************/

/* 
 * File:   PlacesToGo.cpp
 * Author: Heidi Dye
 * 
 * Created on June 6, 2019, 11:26 AM
 */

#include "PlacesToGo.h"


//default constructor
PlacesToGo::PlacesToGo() 
{
    string nameOfPlace = " ";
    double averagePrice = 0.0;
    int howManyPeople = 0;
}

//constructor
PlacesToGo::PlacesToGo(string nameOfPlace, double averagePrice, int howMnayPeople)
{
    this->nameOfPlace = nameOfPlace;
    this->averagePrice = averagePrice;
    this->howManyPeople = howManyPeople;
}

//copy constructor
PlacesToGo::PlacesToGo(const PlacesToGo& placesToGo) 
{
    this->nameOfPlace = placesToGo.nameOfPlace;
    this->averagePrice = placesToGo.averagePrice;
    this->howManyPeople = placesToGo.howManyPeople;
}

//destructor
PlacesToGo::~PlacesToGo() 
{
    
}

//assignment overload
const PlacesToGo& PlacesToGo::operator=(const PlacesToGo& rhs)
{
    //check if assigning to self
    if(this==&rhs)return *this;
    //copy over values
    this->nameOfPlace = rhs.nameOfPlace;
    this->averagePrice = rhs.averagePrice;
    this->howManyPeople = rhs.howManyPeople;
}

//setter member functions
void PlacesToGo::setNameOfPlace(string nameOfPlace)
{
    this->nameOfPlace = nameOfPlace;
}

void PlacesToGo::setAveragePrice(double averagePrice)
{
    this->averagePrice = averagePrice;
}

void PlacesToGo::setHowManyPeople(int howManyPeople)
{
    this->howManyPeople = howManyPeople;
}

//getter member functions
string PlacesToGo::getNameOfPlace()
{
    return nameOfPlace;
}

double PlacesToGo::getAveragePrice()
{
    return averagePrice;
}

int PlacesToGo::getHowManyPeople()
{
    return howManyPeople;
}
    
//output 
void PlacesToGo::output()
{
    cout<<"Name of Place: "<<nameOfPlace<<endl;
    averagePrice = averagePrice * howManyPeople;
    //for fancy output reasons with person vs people
    if(howManyPeople == 1)
    {
        cout<<"Average cost for "<<howManyPeople<<" person is $"<<fixed<<setprecision(2)<<averagePrice<<endl;
    }
    else
    {
        cout<<"Average cost for "<<howManyPeople<<" people is $"<<fixed<<setprecision(2)<<averagePrice<<endl;
    }
    
}