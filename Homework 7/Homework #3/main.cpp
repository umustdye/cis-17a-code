
/* 
 * File:   Homework 7 #3.cpp
 * Author: Heidi Dye
 *
 * Created on May 14, 2019, 10:25 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

//functions
//write array to file
void arrayToFile(string, int*, int);
//read array from file
void fileToArray(string, int*, int);


int main(int argc, char** argv) {

    //get the name of the file
    //cin.ignore();
    string fileName;
    cout<<"Enter the name of the file"<<endl;
    getline(cin, fileName, '\n');
    
    //get the size of the array
    int size;
    cout<<"Enter the size of the array: ";
    cin>>size;
    
    //create the array
    int* array = new int[size];
    
    //dummy value for the array input
    int input;
    
    //fill the array
    for (int i=0; i<size; i++)
    {
        cout<<"Enter value for array element #"<<i+1<<" : ";
        cin>>input;
        array[i]=input;
    }
    
    //write array to file
    arrayToFile(fileName, array, size);
    
    //read the array from the file and output it
    fileToArray(fileName, array, size);
    
    delete[]array;
    array = NULL;
    return 0;
}

void arrayToFile(string fileName, int* array, int size)
{
    //create an object to open and close the file
    fstream file;
    
    cout<<"Writing array to file..."<<endl;
    
    //open the file
    file.open(fileName, ios::out | ios::binary);
    
    //write the array to the file
    file.write(reinterpret_cast<char *>(&array), sizeof(array));
    
    //close the file
    file.close();
}

void fileToArray(string fileName, int* array, int size)
{
    //create an object to open and close the file
    fstream file;
    
    //open the file
    file.open(fileName, ios::in | ios::binary);
    
    //read the file
    file.read(reinterpret_cast<char *>(&array), sizeof(array));
    
    cout<<"Read the file..."<<endl;
    
    //output the array
    for(int i = 0; i<size; i++)
    {
        cout<<array[i]<<" ";
    }
    cout<<endl;
    //close the file
    file.close();
            
}
