

/* 
 * File:   Homework 7 #1 and #2.cpp
 * Author: Heidi Dye
 *
 * Created on May 14, 2019, 9:25 PM
 */

#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

//funtions
void menu();
bool reanimator();
void writeFile();
void encryptFile();
void decodeFile();

int main(int argc, char** argv) {
    //go to the menu for the first time
    menu();
    
    //return to the menu after each run through until the user exits using the 
    // reanimator 
    while(reanimator())
    {
        menu();
    }
    return 0;
}

void menu()
{
    int option=0;
    
    cout << "File Encryption and Decoder\n" <<endl;
    cout << "Please select the number of a corresponding choice from one of the options below: \n";
    cout << "--------------------------------------------------\n";
    //if the user has not made a text file to encrypt/decode
    cout << "1. Create a brand new text file ~"<<endl;
    //encrypt a text file
    cout << "2. HW Problem 1: File Encryption Filter ~"<<endl;
    //decrypt a text file
    cout << "3. HW Problem 2: File Decryption Filter ~"<<endl;
            
    cin >> option;
    
    //check if the option is numerical
    bool valid;
    valid = option;
    if (!valid)
    {
        cin.clear ();
        cin.ignore (10000, '\n');
        cout<< "Sorry, invalid input. Please enter a numerical value between 1 - 4\n";
        exit(1); 
    }
    
    if (option == 1)
    {
        writeFile();
    }
    
    if (option == 2)
    {
        encryptFile(); 
    }
    
    if (option == 3)
    {
        decodeFile();
    }
    
                 
}

bool reanimator ()
{
    char choice, Y,y;
    cout << "Would you like to select another option from the main menu?\n";
    cout<< "PRESS Y TO CONTINUE, OR ANY OTHER KEY TO QUIT\n";
    cin>> choice; 
    if( choice == 'y' || choice == 'Y') return true; //go back to the main menu
    else return false; //exit the menu option  
}

void writeFile()
{
    //ask the user for the name of the file they wish to create
    cin.ignore();
    string filename;
    cout << "Enter name of the file you want to save options to: ";
    getline(cin, filename, '\n');  
    
    //get a string from the user
    char textForFile[300];
    
    cout<<"Enter the text you wish to write to your file..."<<endl;
    gets(textForFile);
    
    //create the object to save the file
    ofstream outFile;   
    
    //open the file
    outFile.open(filename.c_str()); 
    
    //write the data to the file
    if (outFile)
    {
       //write the text from the user to the file
        outFile << textForFile << endl;
        

        //close the file
        outFile.close();

        cout << "Wrote to file: " << filename << endl;

    }
    
    //if there was an error in writing to the file
    else 
    {
        cout << "Could not write data to " <<filename << endl;
        exit(1);
    }

}

void encryptFile()
{
    //hold a character from the file
    char read;
    
    //get the file name from the user
    string fileName;
    cin.ignore();
    cout<<"Enter the name of the file you wish to Encrypt."<<endl;
    getline(cin, fileName, '\n');
    
    //get the name of the file the user wants to write the encryption to
    string encryptName;
    cout<<"Enter the name of the file you wish to write the encrypted file to."<<endl;
    getline(cin, encryptName, '\n');
    ifstream inFile;
    ofstream outFile;
    
    //open the files
    inFile.open(fileName.c_str());
    outFile.open(encryptName.c_str());
    
    
    if(inFile)
    {

        //get the info from the file
        inFile.get(read);
    
        while(!(inFile.eof()))
        {
            //add 10 to each digit
            read = read + 10;
        
            //write to the encrypted file
            outFile.put(read);
        
            //get the next character
            inFile.get(read);
        }
    
        //close the files
        inFile.close();
        outFile.close();
    
        cout << "File has been encrypted and written to "<<encryptName<<endl;
        }
    else
    {
        cout<<"Could not read or write to file..."<<endl;
        exit(1);
    }
    
}

void decodeFile()
{

    //hold a character from the file
    char read;
    
    //get the file name from the user
    string fileName;
    cin.ignore();
    cout<<"Enter the name of the encrypted file you wish to decrypt."<<endl;
    getline(cin, fileName, '\n');
    
    //get the name of the file the user wants to write the decryption to
    string decryptName;
    cout<<"Enter the name of the file you wish to write the decrypted file to."<<endl;
    getline(cin, decryptName, '\n');
    ifstream inFile;
    ofstream outFile;
    
    //open the files
    inFile.open(fileName.c_str());
    outFile.open(decryptName.c_str());
    
    
    if(inFile)
    {

        //get the info from the file
        inFile.get(read);
    
        while(!(inFile.eof()))
        {
            //subtract 10 to each digit
            read = read - 10;
        
            //write to the decrypted file
            outFile.put(read);
        
            //get the next character
            inFile.get(read);
        }
    
        //close the files
        inFile.close();
        outFile.close();
    
        cout << "File has been decrypted and written to "<<decryptName<<endl;
        }
    else
    {
        cout<<"Could not read or write to file..."<<endl;
        exit(1);
    }
        
}