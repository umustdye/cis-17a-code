
/* 
 * File:   Homework 4.cpp
 * Author: Heidi Dye
 *
 * Created on April 29, 2019, 2:27 PM
 */

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <iomanip>

using namespace std;

//classes
//#3 Date
class Date
{
private:
    int month;
    int day;
    int year;
public:
    //getter
    int getMonth();
    int getDay();
    int getYear();
    //setter
    void setMonth(int);
    void setDay(int);
    void setYear(int);
};

//#4 Employee
class Employee
{
private:
    //employee's name
    string name;
    //employee ID number
    int idNumber;
    //department where the employee works
    string department;
    //employee's job title
    string position;
public:
    //constructors
    //default constructor
    Employee();
    //constructor without department and position;
    Employee(string name, int idNumber);
    //construct with all the variables
    Employee(string name, int idNumber, string department, string position);
    //getter functions
    string getName();
    int getIDNumber();
    string getDepartment();
    string getPosition();
};
//#5 Car
class Car
{
private:
    //car's year model
    int yearModel;
    //make of the car
    string make;
    //car's current speed
    int speed;
    
public:
    //default constructor
    Car();
    //constructor
    Car(int yearModel, string make);
    //getter member functions
    int getYearModel();
    string getMake();
    int getSpeed();
    void accelerate(int speed);
    void brake(int speed);
};
//#6 Inventory 
class Inventory
{
private:
    //item's item number
    int itemNumber;
    //quantity of items on hand
    int quantity;
    //wholesale per-unit cost of the item
    double cost;
    //total inventory cost of the item (quantity * cost)
    double totalCost;
public:
    //default constructor
    Inventory ();
    //constructor #2
    Inventory(int itemNumber, int quantity, double cost);
    //setter member functions
    void setItemNumber(int itemNumer);
    void setQuantity(int quantity);
    void setCost(double cost);
    double setTotalCost();
    //getter member functions
    int getItemNumber();
    int getQuantity();
    double getCost();
    double getTotalCost();
        
};

//structs
//#1 soccer score
struct SoccerScore
{
    string playerName;
    int playerNumber;
    //points scored by player
    int playerScore;
};
//#2 Weather Statistics Modification
struct WeatherStatistics
{
    double totalRainfall;
    double highTemp;
    double lowTemp;
    double averageTemp;
};

//enums
//#2 Weather Statistics Modification
enum Month {JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER};

//functions
void one();
void two();
void three();
void four();
void five();
void six();

int main(int argc, char** argv) {

    one();
    two();
    three();
    four();
    five();
    six();
    return 0;
}

void one()
{
    //pg 660
    cout << "#1: Soccer Scores" << endl;
    //there are only three players
    //just in case the number of players need to change
    int teamSize = 3;
    //create an array of the struct SoccerScore with 3 elements for the players
    SoccerScore soccerScore[teamSize];
    
    //have the user enter the name, number, and score for all the players
    cout << "Enter the following information..." << endl;
    for (int i = 0; i<teamSize; i++)
    {
        cout << "Player #" <<i+1<<" name: ";
        cin >> soccerScore[i].playerName;
        cout << "Player #"<<i+1<< " number: ";
        cin >> soccerScore[i].playerNumber;
        //input validation... no negatives!
        if (soccerScore[i].playerNumber < 0)
        {
            cout<<"Sorry. Input must be greater than 0..." <<endl;
            exit(1);
                    
        }
        cout << "Player #"<<i+1<<" total points earned: ";
        cin >> soccerScore[i].playerScore;
        //input validation... no negatives!
        if (soccerScore[i].playerScore < 0)
        {
            cout<<"Sorry. Input must be greater than 0..." <<endl;
            exit(1);
                    
        }
    }
    cout <<endl;
    //find the total score for the entire team
    int totalPoints = 0;
    for (int i = 0; i<teamSize; i++)
    {
        totalPoints = totalPoints + soccerScore[i].playerScore;
    }
    
    //find the player who scored the most points
    int highestScore = 0;
    int highestScoringPlayer = 0;
    
    for (int i = 0; i<teamSize; i++)
    {
        if (soccerScore[i].playerScore > highestScore)
        {
            highestScore = soccerScore[i].playerScore;
            highestScoringPlayer = i;
        }
    }
    
    //Display a table that shows all the players' names, numbers, and scores. 
    //As well as the player with the most points and the team's total score.

    for (int i = 0; i<teamSize; i++)
    {
        cout << "Player #"<<i+1<<"..."<<endl;
        cout <<"Player Name: "<<soccerScore[i].playerName<< endl;
        cout <<"Player Number: "<<soccerScore[i].playerNumber<<endl;
        cout <<"Player Score: "<<soccerScore[i].playerScore<<endl;
    }
    cout << "Team's total score: "<<totalPoints<<endl;
    cout << "Player with the highest score: "<<soccerScore[highestScoringPlayer].playerName<<endl;
    cout << "Their Number: "<<soccerScore[highestScoringPlayer].playerNumber<<endl;
    cout << "Their Score: "<<highestScore<<endl;
}

void two()
{
    //pg 660
    cout << "#2: Weather Statistics Modification " <<endl;
     //create the weatherstatistics array with 12 elements
    WeatherStatistics weatherStatistics[12];
    double maxRange = 140;
    double minRange = -100;
    
    //create a enum variable for Month
    Month month;
    
    //fill the array with data
    //use enum month to step through the array
    for (Month month = JANUARY; month<=DECEMBER; month = static_cast<Month>(month+1))
    {
        //January
        if (month == 0)
        {
            cout << "Enter the following for the month of January..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
        
        //February
        if (month == 1)
        {
            cout << "Enter the following for the month of February..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
    
        }
        
        //March
        if (month == 2)
        {
            cout << "Enter the following for the month of March..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
        
        //April
        if (month == 3)
        {
            cout << "Enter the following for the month of April..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
    
        }
        
        //May
        if (month == 4)
        {
            cout << "Enter the following for the month of May..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
        
        //June
        if (month == 5)
        {
            cout << "Enter the following for the month of June..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
    
        }
        
        //July
        if (month == 6)
        {
            cout << "Enter the following for the month of July..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;

        }
        
        //August
        if (month == 7)
        {
            cout << "Enter the following for the month of August..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
            
        //September
        if (month == 8)
        {
            cout << "Enter the following for the month of September..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
    
        }
        
        //October
        if (month == 9)
        {
            cout << "Enter the following for the month of October..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
        
        //November
        if (month == 10)
        {
            cout << "Enter the following for the month of November..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
        
        //December
        if (month == 11)
        {
            cout << "Enter the following for the month of December..." <<endl;
            cout << "Total Rainfall: ";
            cin >> weatherStatistics[month].totalRainfall;
            cout << "Highest Temperature: ";
            cin >> weatherStatistics[month].highTemp;
            cout << "Lowest Temperature: ";
            cin >> weatherStatistics[month].lowTemp;
            cout << "Average Temperature: ";
            cin >> weatherStatistics[month].averageTemp;
        }
            //input validation
            if(weatherStatistics[month].highTemp < minRange || maxRange < weatherStatistics[month].highTemp)
            {
                cout << "ERROR! Highest Temperature must be between the range of -100 and 140 degrees Fahrenheit..."<<endl;
                exit(1);
            }
            if(weatherStatistics[month].lowTemp < minRange || weatherStatistics[month].lowTemp > maxRange)
            {
                cout << "ERROR! Lowest Temperature must be between the range of -100 and 140 degrees Fahrenheit..."<<endl;
                exit(1);
            }
            if(weatherStatistics[month].averageTemp < minRange || weatherStatistics[month].averageTemp > maxRange)
            {
                cout << "ERROR! Average Temperature must be between the range of -100 and 140 degrees Fahrenheit..."<<endl;
                exit(1);
            }
    }
    
    //calculate the total rainfall
    double totalRainFall = 0;
    for (Month month = JANUARY; month <= DECEMBER; month = static_cast<Month>(month+1))
    {
        totalRainFall = totalRainFall + weatherStatistics[month].totalRainfall;
    }
    
    //calculate total average temperature
    double totalAverageRainFall = 0;
    totalAverageRainFall = totalRainFall/12;
    
    //calculate the highest temp
    double highestTemp = -100;
    int highestTempMonth =0;
    for (Month month = JANUARY; month<= DECEMBER; month = static_cast<Month>(month +1))
    {
        int i = month;
        if (highestTemp < weatherStatistics[i].highTemp)
        {
            highestTempMonth =i+1;
            highestTemp = weatherStatistics[i].highTemp;
        }
       
    }
    string hTempMonth;
    //switch the int month into a string
    switch (highestTempMonth)
    {
        case 1:
            hTempMonth = "January";
            break;
        case 2:
            hTempMonth = "February";
            break;
        case 3:
            hTempMonth = "March";
            break;
        case 4:
            hTempMonth = "April";
            break;
        case 5:
            hTempMonth = "May";
            break;
        case 6:
            hTempMonth = "June";
            break;
        case 7:
            hTempMonth = "July";
            break;
        case 8:
            hTempMonth = "August";
            break;
        case 9:
            hTempMonth = "September";
            break;
        case 10:
            hTempMonth = "October";
            break;
        case 11:
            hTempMonth = "November";
            break;
        case 12:
            hTempMonth = "December";
            break;
            
    }
    
    //calculate lowest temp
    double lowestTemp = 140;
    int lowestTempMonth = 0;
    for (Month month  = JANUARY; month<= DECEMBER; month = static_cast<Month>(month+1))
    {
        int i = month;
        if (lowestTemp > weatherStatistics[i].lowTemp)
        {
            lowestTempMonth = i+1;
            lowestTemp = weatherStatistics[i].lowTemp;
        }
    }
    
    string lTempMonth;
    //switch the int month into a string
    switch (lowestTempMonth)
    {
        case 1:
            lTempMonth = "January";
            break;
        case 2:
            lTempMonth = "February";
            break;
        case 3:
            lTempMonth = "March";
            break;
        case 4:
            lTempMonth = "April";
            break;
        case 5:
            lTempMonth = "May";
            break;
        case 6:
            lTempMonth = "June";
            break;
        case 7:
            lTempMonth = "July";
            break;
        case 8:
            lTempMonth = "August";
            break;
        case 9:
            lTempMonth = "September";
            break;
        case 10:
            lTempMonth = "October";
            break;
        case 11:
            lTempMonth = "November";
            break;
        case 12:
            lTempMonth = "December";
            break;
            
    }
    
    //calculate average temp
    double totalAverageTemp = 0;
    for (Month month = JANUARY; month<= DECEMBER; month = static_cast<Month>(month+1))
    {
        int i =month;
        totalAverageTemp = totalAverageTemp + weatherStatistics[i].averageTemp;
    }
    
    totalAverageTemp = totalAverageTemp / 12;
    
    //output the data
    cout<<"Total Rainfall: "<<totalRainFall<<endl;
    cout<<"Average Rainfall: "<<totalAverageRainFall<<endl;
    cout<<"Highest Temperature: "<<highestTemp<<" in the month of "<<hTempMonth<<endl;
    cout<<"Lowest Temperature: "<<lowestTemp<<" in the month of "<<lTempMonth<<endl;
    cout<<"Average Temperature: "<<totalAverageTemp<<endl;
}

void three()
{
    //p 808
    cout <<"#3: Date"<<endl;
    //create a date object for the class Date
    Date date;
    cout << "Enter the following...(numerical values only)"<<endl;
    //ask the user for the month
    cout << "Month: ";
    int month = 0;
    cin >> month;
    //input validation
    if (month <1 || month>12)
    {
        cout << "Invalid input... "<<endl;
        exit(1);
    }
    //set the year for the class
    date.setMonth(month);
    
    //ask the user for the day
    cout<<"Day: ";
    int day = 0;
    cin >> day;
    //input validation
    if (day<1 || day >31)
    {
        cout <<"invalid input..."<<endl;
        exit(1);
    }
    //set the day for the class
    date.setDay(day);
    //ask the user for the year
    cout <<"Year: ";
    int year = 0;
    cin >> year;
    //set the year for the class
    date.setYear(year);
    
    //create a string that represents the month
    string monthstring;
    // switch the int month into a string
    switch (date.getMonth())
    {
        case 1:
            monthstring = "January";
            break;
        case 2:
            monthstring = "February";
            break;
        case 3:
            monthstring = "March";
            break;
        case 4:
            monthstring = "April";
            break;
        case 5:
            monthstring = "May";
            break;
        case 6:
            monthstring = "June";
            break;
        case 7:
            monthstring = "July";
            break;
        case 8:
            monthstring = "August";
            break;
        case 9:
            monthstring = "September";
            break;
        case 10:
            monthstring = "October";
            break;
        case 11:
            monthstring = "November";
            break;
        case 12:
            monthstring = "December";
            break;
    }
    //output the date
    //12/25/2018 form
    cout<<date.getMonth()<<"/"<<date.getDay()<<"/"<<date.getYear()<<endl;
    //December 25, 2018 form
    cout<<monthstring<<" "<<date.getDay()<<", "<<date.getYear()<<endl;
    // 25 December 2018 form
    cout<<date.getDay()<<" "<<monthstring<<" "<<date.getYear()<<endl;
}

int Date::getMonth()
{
    return month;
}

int Date::getDay()
{
    return day;
}

int Date::getYear()
{
    return year;
}

void Date::setMonth(int month)
{
    this->month = month;
}

void Date::setDay(int day)
{
    this->day = day;
}

void Date::setYear(int year)
{
    this->year = year;
}

void four()
{
    //pg. 808
    cout<<"#4: Employee Class"<<endl;
    //with the data included with the problem
    Employee employee1 ("Susan Meyers", 47899, "Accounting", "Vice President");
    Employee employee2 ("Mark Jones", 39119, "IT", "Programmer");
    Employee employee3 ("Joy Rogers", 81774, "Manufacturing", "Engineer");
    
    //output the employee info
    cout <<"Employee #1..."<<endl;
    cout <<"Name: "<<employee1.getName()<<endl;
    cout <<"ID Number: "<<employee1.getIDNumber()<<endl;
    cout <<"Department: "<<employee1.getDepartment()<<endl;
    cout <<"Position: "<<employee1.getPosition()<<endl;
        
    cout <<"Employee #2..."<<endl;
    cout <<"Name: "<<employee2.getName()<<endl;
    cout <<"ID Number: "<<employee2.getIDNumber()<<endl;
    cout <<"Department: "<<employee2.getDepartment()<<endl;
    cout <<"Position: "<<employee2.getPosition()<<endl;
        
    cout <<"Employee #3..."<<endl;
    cout <<"Name: "<<employee3.getName()<<endl;
    cout <<"ID Number: "<<employee3.getIDNumber()<<endl;
    cout <<"Department: "<<employee3.getDepartment()<<endl;
    cout <<"Position: "<<employee3.getPosition()<<endl;
}

//default constructor
Employee::Employee()
{
    name = "";
    idNumber = 0;
    department = "";
    position = "";
}

//constructor without position and department
Employee::Employee(string name, int idNumber)
{
    this->name =name;
    this->idNumber = idNumber;
    position = "";
    department = "";
}

//constructor with name, ID, position, and department
Employee::Employee(string name, int idNumber, string department, string position)
{
    this->name = name;
    this->idNumber = idNumber;
    this->department = department;
    this->position = position;
}

string Employee::getName()
{
    return name;
}

int Employee::getIDNumber()
{
    return idNumber;
}

string Employee::getDepartment()
{
    return department;
}

string Employee::getPosition()
{
    return position;
}

void five()
{
    //pg. 808-809
    cout<<"#5: Car Class"<<endl;
    //if you want the user to enter the make and model info
    /*cout<<"What year was the car released: ";
    int yearModel = 0;
    cin >> yearModel;
    cout<<"What is the make of the car: ";
    string make;
    cin >> make;
    //make the car object
    Car car(yearModel, make);
    */
    //if you want prefilled info
     Car car(1969, "Volkswagen Beetle");
    
     //output the make and model of the car
     cout<<"Make of the car: "<<car.getMake()<<endl;
     cout<<"Year of the model: "<<car.getYearModel()<<endl;
    //get the speed
    int speed = car.getSpeed();
    cout <<"Speed before: "<<speed<<endl;
    //Accelerate the car five times
    for (int i=1; i<6; i++)
    {
        car.accelerate(speed);
        speed = car.getSpeed();
        cout<<"Current speed after accelerating: "<<speed<<endl;
    }
    
    //brake the car five times
    for (int i = 1; i<6; i++)
    {
        car.brake(speed);
        speed = car.getSpeed();
        cout<<"Current speed after braking: "<<speed<<endl;
    }
}
//default constructor
Car::Car()
{
    yearModel = 0;
    make = "";
    speed = 0;
}
//constructor for car class
Car::Car(int yearModel, string make)
{
    this->yearModel = yearModel;
    this->make= make;
    speed = 0;
}

//getter member functions for Car class
int Car::getYearModel()
{
    return yearModel;
}

string Car::getMake()
{
    return make;
}

int Car::getSpeed()
{
    return speed;
}

//adds five to the speed every time the function called
void Car::accelerate(int speed)
{
    this->speed = speed+5;
}

//subtracts five from the speed every time the function is called
void Car::brake(int speed)
{
    this->speed = speed-5;
}

void six()
{
    //pg. 810
    //The problem wasn't too specific on what the main function should be 
    //doing, so I winged it.
    cout<<"#6: Inventory Class"<<endl;
    //get the cost, item number, and quantity form the user
    cout<<"Item Number: ";
    int itemNumber;
    cin>>itemNumber;
    cout<<"Quantity: ";
    int quantity;
    cin>>quantity;
    cout<<"Cost: $";
    double cost;
    cin>>cost;
    //input validation
    if((cost<0)||(quantity<0)||(itemNumber<0))
    {
        cout<<"Error! Input must be greater than zero..."<<endl;
        exit(1);
    }
    //create an Inventory Class object
    Inventory inventory(itemNumber, quantity, cost);
    //obtain the total cost from the class
    double totalCost = inventory.getTotalCost();
    cout <<"Total Cost: $"<<fixed<<setprecision(2)<<totalCost<<endl;
    
}
//default constructor
Inventory::Inventory()
{
    itemNumber = 0;
    quantity = 0;
    cost = 0;
    totalCost = 0;
}
//2nd constructor
Inventory::Inventory(int itemNumber, int quantity, double cost)
{
    this->itemNumber = itemNumber;
    this->quantity = quantity;
    this->cost = cost;
    setTotalCost();
}

//setter member functions
void Inventory::setItemNumber(int itemNumber)
{
    this->itemNumber = itemNumber;
}
void Inventory::setQuantity(int quantity)
{
    this->quantity = quantity;
}
void Inventory::setCost(double cost)
{
    this->cost = cost;
}
double Inventory::setTotalCost()
{
    this->totalCost = cost*quantity;
}

//getter member functions
int Inventory::getItemNumber()
{
    return itemNumber;
}
int Inventory::getQuantity()
{
    return quantity;
}
double Inventory::getCost()
{
    return cost;
}
double Inventory::getTotalCost()
{
    return totalCost;
}